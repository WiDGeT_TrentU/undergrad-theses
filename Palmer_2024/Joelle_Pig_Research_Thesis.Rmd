---
title: "Joelle_Pig_Research_Thesis"
author: "Joelle Palmer - 0742208"
date: '2024-02-08'
output: pdf_document
---

#Calculate Accumulated Degree Days (ADD) - Using Megyesi et. al Equation 
```{r}
#Megyesi et. al Equation for ADD:
# 10^(0.002*TBS*TBS+1.81) +/- 388.19 
#import the dataset (.csv)
TBS.ADD.data<-read.csv('~/Desktop/Research Thesis/Pig.Data.csv')
TBS.ADD.data

install.packages("dplyr")
library(dplyr)

#Isolate TBS for each pig for each scoring system.
Pigs.TBS<-select(TBS.ADD.data,Pig_ID,TBS_Megyesi, TBS_Keough, TBS_Ribereau.Gayon) 

#For Pig 01
Pig01_TBS_Megyesi<-subset(Pigs.TBS, Pig_ID == "1", TBS_Megyesi)
Pig01_TBS_Keough<-subset(Pigs.TBS, Pig_ID == "1", TBS_Keough)
Pig01_TBS_Ribereau.Gayon<-subset(Pigs.TBS, Pig_ID == "1", TBS_Ribereau.Gayon)

#For Pig 02
Pig02_TBS_Megyesi<-subset(Pigs.TBS, Pig_ID == "2", TBS_Megyesi)
Pig02_TBS_Keough<-subset(Pigs.TBS, Pig_ID == "2", TBS_Keough)
Pig02_TBS_Ribereau.Gayon<-subset(Pigs.TBS, Pig_ID == "2", TBS_Ribereau.Gayon)

#For Pig 03
Pig03_TBS_Megyesi<-subset(Pigs.TBS, Pig_ID == "3", TBS_Megyesi)
Pig03_TBS_Keough<-subset(Pigs.TBS, Pig_ID == "3", TBS_Keough)
Pig03_TBS_Ribereau.Gayon<-subset(Pigs.TBS, Pig_ID == "3", TBS_Ribereau.Gayon)

#For Pig 04
Pig04_TBS_Megyesi<-subset(Pigs.TBS, Pig_ID == "4", TBS_Megyesi)
Pig04_TBS_Keough<-subset(Pigs.TBS, Pig_ID == "4", TBS_Keough)
Pig04_TBS_Ribereau.Gayon<-subset(Pigs.TBS, Pig_ID == "4", TBS_Ribereau.Gayon)

#ADD Calculation for Megyesi et. al
Pig01_M_ADD_Meg<-10^(0.002*Pig01_TBS_Megyesi*Pig01_TBS_Megyesi+1.81)
Pig02_M_ADD_Meg<-10^(0.002*Pig02_TBS_Megyesi*Pig02_TBS_Megyesi+1.81)
Pig03_M_ADD_Meg<-10^(0.002*Pig03_TBS_Megyesi*Pig03_TBS_Megyesi+1.81)
Pig04_M_ADD_Meg<-10^(0.002*Pig04_TBS_Megyesi*Pig04_TBS_Megyesi+1.81)

#ADD Calculation for Keough et. al
Pig01_K_ADD_Meg<-10^(0.002*Pig01_TBS_Keough*Pig01_TBS_Keough+1.81)
Pig02_K_ADD_Meg<-10^(0.002*Pig02_TBS_Keough*Pig02_TBS_Keough+1.81)
Pig03_K_ADD_Meg<-10^(0.002*Pig03_TBS_Keough*Pig03_TBS_Keough+1.81)
Pig04_K_ADD_Meg<-10^(0.002*Pig04_TBS_Keough*Pig04_TBS_Keough+1.81)

#ADD Calculation for Ribereau-Gayon et. al
Pig01_RB_ADD_Meg<-10^(0.002*Pig01_TBS_Ribereau.Gayon*Pig01_TBS_Ribereau.Gayon+1.81)
Pig02_RB_ADD_Meg<-10^(0.002*Pig02_TBS_Ribereau.Gayon*Pig02_TBS_Ribereau.Gayon+1.81)
Pig03_RB_ADD_Meg<-10^(0.002*Pig03_TBS_Ribereau.Gayon*Pig03_TBS_Ribereau.Gayon+1.81)
Pig04_RB_ADD_Meg<-10^(0.002*Pig04_TBS_Ribereau.Gayon*Pig04_TBS_Ribereau.Gayon+1.81)
```

#Calculate Accumulated Degree Days (ADD) - Using Moffatt et. al Equation 
```{r}
#Moffatt et. al Equation : 
#TBSsurf^1.6=125(log10(ADD))-212
                                #Isolate 212
#TBSsurf^1.6 + 212=125(log10(ADD))
                                #Divide by 125
#(TBSsurf^1.6 + 212)/125=(log10(ADD))
                                #Remove log (^10)
#10^(((TBSsurf^1.6) + 212)/125)=ADD
                                #Switch sides
# ADD = 10^(((TBSsurf^1.6) + 212)/125)

#Megyesi et. al ADD calculation
Pig01_M_ADD_Moff<-10^(((Pig01_TBS_Megyesi^1.6) + 212)/125)
Pig02_M_ADD_Moff<-10^(((Pig02_TBS_Megyesi^1.6) + 212)/125)
Pig03_M_ADD_Moff<-10^(((Pig03_TBS_Megyesi^1.6) + 212)/125)
Pig04_M_ADD_Moff<-10^(((Pig04_TBS_Megyesi^1.6) + 212)/125)

#Keough et. al Pig TBS Data ADD calculation
Pig01_K_ADD_Moff<-10^(((Pig01_TBS_Keough^1.6) + 212)/125)
Pig02_K_ADD_Moff<-10^(((Pig02_TBS_Keough^1.6) + 212)/125)
Pig03_K_ADD_Moff<-10^(((Pig03_TBS_Keough^1.6) + 212)/125)
Pig04_K_ADD_Moff<-10^(((Pig04_TBS_Keough^1.6) + 212)/125)

#Ribereau-Gayon et. al Pig TBS Data ADD calculation
Pig01_RB_ADD_Moff<-10^(((Pig01_TBS_Ribereau.Gayon^1.6) + 212)/125)
Pig02_RB_ADD_Moff<-10^(((Pig02_TBS_Ribereau.Gayon^1.6) + 212)/125)
Pig03_RB_ADD_Moff<-10^(((Pig03_TBS_Ribereau.Gayon^1.6) + 212)/125)
Pig04_RB_ADD_Moff<-10^(((Pig04_TBS_Ribereau.Gayon^1.6) + 212)/125)
```

#Calculate 'TRUE' Degree Days - Using Wahyono et. al
```{r}
#Equation:
# ADD = [(Temp_Min+Temp_Max)/2] - threshold 
                       # threshold = -5c (for biological processes)

#Import Monthly Temperature Data 
Temp.Oct.2023<-read.csv('~/Desktop/Research Thesis/Oct.2023.csv')
Temp.Nov.2023<-read.csv('~/Desktop/Research Thesis/Nov02.2023.csv')
Temp.Dec.2023<-read.csv('~/Desktop/Research Thesis/Dec02.2023.csv')
Temp.Jan.2024<-read.csv('~/Desktop/Research Thesis/Jan.2024.csv')
Temp.Feb.2024<-read.csv('~/Desktop/Research Thesis/Feb.2024.csv')

#Isolate Min and Max Temperatures from each month
Min.Temp.Oct.2023<-Temp.Oct.2023$Min.Temp...C.
Min.Temp.Oct.2023.Dates<-Min.Temp.Oct.2023[26:31]  

Min.Temp.Nov.2023<-Temp.Nov.2023$Min.Temp...C.
Min.Temp.Nov.2023.Dates<-Min.Temp.Nov.2023[1:31]

Min.Temp.Dec.2023<-Temp.Dec.2023$Min.Temp...C.
Min.Temp.Dec.2023.Dates<-Min.Temp.Dec.2023[1:31]

Min.Temp.Jan.2024<-Temp.Jan.2024$Min.Temp...C.
Min.Temp.Jan.2024.Dates<-Min.Temp.Jan.2024[1:25] 

Min.Temp.Feb.2024<-Temp.Feb.2024$Min.Temp...C.
Min.Temp.Feb.2024.Dates<-Min.Temp.Feb.2024[1:8] 

Max.Temp.Oct.2023<-Temp.Oct.2023$Max.Temp...C.
Max.Temp.Oct.2023.Dates<-Max.Temp.Oct.2023[26:31]  

Max.Temp.Nov.2023<-Temp.Nov.2023$Max.Temp...C.
Max.Temp.Nov.2023.Dates<-Max.Temp.Nov.2023[1:31]    

Max.Temp.Dec.2023<-Temp.Dec.2023$Max.Temp...C.
Max.Temp.Dec.2023.Dates<-Max.Temp.Dec.2023[1:31]   

Max.Temp.Jan.2024<-Temp.Jan.2024$Max.Temp...C.
Max.Temp.Jan.2024.Dates<-Max.Temp.Jan.2024[1:25]    

Max.Temp.Feb.2024<-Temp.Feb.2024$Max.Temp...C.
Max.Temp.Feb.2024.Dates<-Max.Temp.Feb.2024[1:8] 

#Find average and isolate those temperature-days above threshold 
Avg.Temp.Oct.2023<-((Min.Temp.Oct.2023.Dates+Max.Temp.Oct.2023.Dates)/2)
Avg.Temp.Nov.2023<-((Min.Temp.Nov.2023.Dates+Max.Temp.Nov.2023.Dates)/2)
Avg.Temp.Dec.2023<-((Min.Temp.Dec.2023.Dates+Max.Temp.Dec.2023.Dates)/2)
Avg.Temp.Jan.2024<-((Min.Temp.Jan.2024.Dates+Max.Temp.Jan.2024.Dates)/2)
Avg.Temp.Feb.2024<-((Min.Temp.Feb.2024.Dates+Max.Temp.Feb.2024.Dates)/2)

#Make Temp interval based on exact sampling dates:
Calculated_Temp.PMI<-c(Avg.Temp.Oct.2023,Avg.Temp.Nov.2023, Avg.Temp.Dec.2023,Avg.Temp.Jan.2024, Avg.Temp.Feb.2024)
Calculated_Temp.PMI
length(Calculated_Temp.PMI)

#plot daily temperature 
pdf("Joelle_Temperature.pdf")
library(ggplot2)
PMI<-c(1:101) # adjust to length 
length(PMI)
ggplot(data=NULL,aes(x=PMI, y=Calculated_Temp.PMI))+geom_line()+geom_hline(yintercept=-5,linetype=2, color="darkblue")+geom_hline(yintercept=0,linetype=2, color="darkblue")+ggtitle("Average Daily Temperature versus PMI")+xlab("Post-Mortem Interval (Days)")+ylab("Temperature (Celcius)")
dev.off

#Isolate those greater than threshold (0C)
Temp.Oct.2023.Thres<-Avg.Temp.Oct.2023[Avg.Temp.Oct.2023 > (-6)]
Temp.Nov.2023.Thres<-Avg.Temp.Nov.2023[Avg.Temp.Nov.2023 > (-6)]
Temp.Dec.2023.Thres<-Avg.Temp.Dec.2023[Avg.Temp.Dec.2023 > (-6)]
Temp.Jan.2024.Thres<-Avg.Temp.Jan.2024[Avg.Temp.Jan.2024 > (-6)]
Temp.Feb.2024.Thres<-Avg.Temp.Feb.2024[Avg.Temp.Feb.2024 > (-6)]

#Subtract threshold Calculate the ADD for each day  
threshold<-(-5)
Add.Oct<-(Temp.Oct.2023.Thres-threshold)
Add.Nov<-(Temp.Nov.2023.Thres-threshold)
Add.Dec<-(Temp.Dec.2023.Thres-threshold)
Add.Jan<-(Temp.Jan.2024.Thres-threshold)
Add.Feb<-(Temp.Feb.2024.Thres-threshold)

#Get postive values
Add.Oct.2023<-abs(Add.Oct)
Add.Nov.2023<-abs(Add.Nov)
Add.Dec.2023<-abs(Add.Dec)
Add.Jan.2024<-abs(Add.Jan)
Add.Feb.2024<-abs(Add.Feb)

#Make ADD interval based on exact sampling dates:
#For Oct. 26
Sample01date.ADD<-sum(Add.Oct.2023[1])
#For Oct. 27  to Nov. 02
Sample02date.ADD<-sum(Add.Oct.2023[2:6]+Add.Nov.2023[1:2])
#For Nov. 03 to Nov. 09
Sample03date.ADD<-sum(Add.Nov.2023[3:9])
#For Nov. 10 to Nov. 16
Sample04date.ADD<-sum(Add.Nov.2023[10:16])         
#For Nov. 17 to Nov. 23
Sample05date.ADD<-sum(Add.Nov.2023[17:23])
#For Nov. 24 to Nov. 30
Sample06date.ADD<-sum(Add.Nov.2023[24:30])
#For Nov. 31 to Dec. 7 
Sample07date.ADD<-sum(Add.Nov.2023[31]+Add.Dec.2023[1:7])
#For Dec. 8 to Dec. 14
Sample08date.ADD<-sum(Add.Dec.2023[8:14])          
#For Dec. 15 to Dec. 21
Sample09date.ADD<-sum(Add.Dec.2023[15:21])
#For Dec. 22 to Jan 04
Sample10date.ADD<-sum(Add.Dec.2023[22:31]+Add.Jan.2024[1:4])
#For Jan 05 to Jan.11
Sample11date.ADD<-sum(Add.Jan.2024[5:11])
#For Jan. 12 to Jan 18
Sample12date.ADD<-sum(Add.Jan.2024[12:18])
#For Jan. 19 to Jan 25
Sample13date.ADD<-sum(Add.Jan.2024[19:25])
#For Jan. 26 to Feb 01
Sample14date.ADD<-sum(Add.Jan.2024[26:31]+Add.Feb.2024[1])
#For Feb. 02 to Feb 08
Sample15date.ADD<-sum(Add.Feb.2024[2:8])

#Altogether-PMI
Calculated_ADD.PMI<-c(Sample01date.ADD, Sample02date.ADD, Sample03date.ADD, Sample04date.ADD, Sample05date.ADD, Sample06date.ADD, Sample07date.ADD, Sample08date.ADD, Sample09date.ADD, Sample10date.ADD, Sample11date.ADD, Sample12date.ADD, Sample13date.ADD,  Sample14date.ADD,  Sample15date.ADD)
Calculated_ADD.PMI

```

#Plotting TBS versus PMI
```{r}
#Isolate Data
Pigs.DNA.TBS.Data<-select(TBS.ADD.data,Sample,PMI,Pig_ID,TBS_Megyesi,TBS_Keough,TBS_Ribereau.Gayon,DNA_Con)
Pigs.DNA.TBS.Data

#Prep
pdf("Joelle-TBS-v-PMI.pdf")
library(ggplot2)

#Build Regression Model for Megyesi et. al (TBS) 
reg_model_Pigs.TBS.Megyesi<-lm((log(TBS_Megyesi+1))~PMI+as.factor(Pig_ID), data = Pigs.DNA.TBS.Data)
coeff_reg_model_Pigs.TBS.Megyesi<-coefficients(reg_model_Pigs.TBS.Megyesi)
int_reg_model_Pigs.TBS.Megyesi<-coeff_reg_model_Pigs.TBS.Megyesi[1]
slope_reg_model_Pigs.TBS.Megyesi<-coeff_reg_model_Pigs.TBS.Megyesi[2]

#Plot
Plot.1<-ggplot(Pigs.DNA.TBS.Data,mapping=aes(x=PMI, y=(log(TBS_Megyesi+1)),color=as.factor(Pig_ID)))+geom_point(position = position_jitter(w=1.3,h=0))+ggtitle("TBS (Megyesi et al.) versus PMI")+geom_abline(intercept=int_reg_model_Pigs.TBS.Megyesi, slope=slope_reg_model_Pigs.TBS.Megyesi, color="black")+xlab("Post-Mortem Interval (Days)")+ylab("log(Total Body Score (TBS))")+ scale_color_brewer(palette="BrBG")+ labs(color='Pig ID')+ylim(1,3)

#Build Regression Model for Keough et. al (TBS) 
reg_model_Pigs.TBS.Keough<-lm((log(TBS_Keough+1))~PMI+as.factor(Pig_ID), data = Pigs.DNA.TBS.Data)
coeff_reg_model_Pigs.TBS.Keough<-coefficients(reg_model_Pigs.TBS.Keough)
int_reg_model_Pigs.TBS.Keough<-coeff_reg_model_Pigs.TBS.Keough[1]
slope_reg_model_Pigs.TBS.Keough<-coeff_reg_model_Pigs.TBS.Keough[2]

#Plot
Plot.2<-ggplot(Pigs.DNA.TBS.Data,mapping=aes(x=PMI, y=(log(TBS_Keough+1)),color=as.factor(Pig_ID)))+geom_point(position = position_jitter(w=1.3,h=0)) +ggtitle("TBS (Keough et al.) versus PMI")+geom_abline(intercept=int_reg_model_Pigs.TBS.Keough, slope=slope_reg_model_Pigs.TBS.Keough, color="black")+xlab("Post-Mortem Interval (Days)")+ylab("log(Total Body Score (TBS))")+ scale_color_brewer(palette="BrBG")+ labs(color='Pig ID')+ylim(1,3)

#Build Regression Model for Ribereau Gayon et. al (TBS) 
reg_model_Pigs.TBS.Ribereau.Gayon<-lm((log(TBS_Ribereau.Gayon+1))~PMI+as.factor(Pig_ID), data = Pigs.DNA.TBS.Data)
coeff_reg_model_Pigs.TBS.Ribereau.Gayon<-coefficients(reg_model_Pigs.TBS.Ribereau.Gayon)
int_reg_model_Pigs.TBS.Ribereau.Gayon<-coeff_reg_model_Pigs.TBS.Ribereau.Gayon[1]
slope_reg_model_Pigs.TBS.Ribereau.Gayon<-coeff_reg_model_Pigs.TBS.Ribereau.Gayon[2]

#Plot
Plot.3<-ggplot(Pigs.DNA.TBS.Data,mapping=aes(x=PMI, y=(log(TBS_Ribereau.Gayon+1)),color=as.factor(Pig_ID)))+geom_point(position = position_jitter(w=1.3,h=0))+ggtitle("TBS (Ribereau Gayon et al.) versus PMI")+geom_abline(intercept=int_reg_model_Pigs.TBS.Ribereau.Gayon, slope=slope_reg_model_Pigs.TBS.Ribereau.Gayon, color="black")+xlab("Post-Mortem Interval (Days)")+ylab("log(Total Body Score (TBS))") + scale_color_brewer(palette="BrBG")+ labs(color='Pig ID')+ylim(1,3)

#put all plots together in one figure
install.packages("cowplot")
library("cowplot")

#put all plots together in one figure
title <- ggdraw() + draw_label("TBS FROM DIFFERENT TBS-SYSTEMS V. PMI (DAYS)", fontface='bold')
plot.grid<-plot_grid(Plot.1, Plot.2, Plot.3, labels=c("A", "B", "C"), ncol = 3, nrow = 1)
plot_grid(title, plot.grid, ncol=1, rel_heights=c(0.1,1))

#dev.off()
```

#Plotting DIN & Peak Ratios versus PMI 
```{r}
#make dataframe of data used 
Pigs.DNA.TapeStation<-select(TBS.ADD.data,Pig_ID, PMI,DIN,DNA_Con, X500_1000, X1000_5000, X5000_10000, X10000_50000)
Pigs.DNA.TapeStation

#Prep
pdf("Joelle-TBS-v-PMI.pdf")
library(ggplot2)

#Build Regression Model
reg_model_DIN<-lm(DIN~PMI+as.factor(Pig_ID), data = Pigs.DNA.TapeStation)
coeff_reg_model_DIN<-coefficients(reg_model_DIN)
int_reg_model_DIN<-coeff_reg_model_DIN[1]
slope_reg_model_DIN<-coeff_reg_model_DIN[2]

#Plot
pdf("Joelle-DIN-v-PMI-Graph.pdf")
pg5<-ggplot(Pigs.DNA.TapeStation,mapping=aes(x=PMI, y=DIN,color=as.factor(Pig_ID)))+geom_point()+ggtitle("DIN versus PMI")+geom_abline(intercept=int_reg_model_DIN, slope=slope_reg_model_DIN, color="black")+xlab("Post-Mortem Interval (Days)")+ylab("DNA Integrity Number (DIN)") + scale_color_brewer(palette="BrBG")+ labs(color='Pig ID') + 
  scale_shape_manual(name = "Pig ID",
                     labels = c("1", "2", "3", "4"),
                     values = c(4,19,19,4)) 

#Build Regression Model for X500_1000 vs PMI
reg_model_X500_1000<-lm(X500_1000~PMI+as.factor(Pig_ID), data = Pigs.DNA.TapeStation)
coeff_reg_model_X500_1000<-coefficients(reg_model_X500_1000)
int_reg_model_X500_1000<-coeff_reg_model_X500_1000[1]
slope_reg_model_X500_1000<-coeff_reg_model_X500_1000[2]
summary(reg_model_X500_1000)
#Plot
pg1<-ggplot(Pigs.DNA.TapeStation,mapping=aes(x=PMI, y=X500_1000,color=as.factor(Pig_ID)))+geom_point()+ggtitle("Fragmentation Region versus PMI")+geom_abline(intercept=int_reg_model_X500_1000, slope=slope_reg_model_X500_1000, color="black")+xlab("Post-Mortem Interval (Days)")+ylab("500_1000 bp Region")+ scale_color_brewer(palette="BrBG")+ labs(color='Pig ID')

#Build Regression Model for X1000_5000 vs PMI
reg_model_X1000_5000<-lm(X1000_5000~PMI+as.factor(Pig_ID), data = Pigs.DNA.TapeStation)
coeff_reg_model_X1000_5000<-coefficients(reg_model_X1000_5000)
int_reg_model_X1000_5000<-coeff_reg_model_X1000_5000[1]
slope_reg_model_X1000_5000<-coeff_reg_model_X1000_5000[2]
summary(reg_model_X1000_5000)
#Plot
pg2<-ggplot(Pigs.DNA.TapeStation,mapping=aes(x=PMI, y=X1000_5000,color=as.factor(Pig_ID)))+geom_point()+ggtitle("Fragmentation Region versus PMI")+geom_abline(intercept=int_reg_model_X1000_5000, slope=slope_reg_model_X1000_5000, color="black")+xlab("Post-Mortem Interval (Days)")+ylab("1000_5000 bp Region")+ scale_color_brewer(palette="BrBG")+ labs(color='Pig ID')

#Build Regression Model for X5000_10000 vs PMI 
reg_model_X5000_10000<-lm(X5000_10000~PMI+as.factor(Pig_ID), data = Pigs.DNA.TapeStation)
coeff_reg_model_X5000_10000<-coefficients(reg_model_X5000_10000)
int_reg_model_X5000_10000<-coeff_reg_model_X5000_10000[1]
slope_reg_model_X5000_10000<-coeff_reg_model_X5000_10000[2]
summary(reg_model_X5000_10000)
#Plot
pg3<-ggplot(Pigs.DNA.TapeStation,mapping=aes(x=PMI, y=X5000_10000,color=as.factor(Pig_ID)))+geom_point()+ggtitle("Fragmentation Region versus PMI")+geom_abline(intercept=int_reg_model_X5000_10000, slope=slope_reg_model_X5000_10000, color="black")+xlab("Post-Mortem Interval (Days)")+ylab("5000_10000 bp Region")+ scale_color_brewer(palette="BrBG")+ labs(color='Pig ID')+ylim(0,20000)

#Build Regression Model for X10000_50000 vs PMI 
reg_model_X10000_50000<-lm(X10000_50000~PMI+as.factor(Pig_ID), data = Pigs.DNA.TapeStation)
coeff_reg_model_X10000_50000<-coefficients(reg_model_X10000_50000)
int_reg_model_X10000_50000<-coeff_reg_model_X10000_50000[1]
slope_reg_model_X10000_50000<-coeff_reg_model_X10000_50000[2]
summary(reg_model_X10000_50000)
#Plot
pg4<-ggplot(Pigs.DNA.TapeStation,mapping=aes(x=PMI, y=X10000_50000,color=as.factor(Pig_ID)))+geom_point()+ggtitle("Fragmentation Region versus PMI")+geom_abline(intercept=int_reg_model_X10000_50000, slope=slope_reg_model_X10000_50000, color="black")+xlab("Post-Mortem Interval (Days)")+ylab("10000_50000 bp Region")+ scale_color_brewer(palette="BrBG")+ labs(color='Pig ID')+ylim(15000,22000)

#put all plots together in one figure
install.packages("cowplot")
library("cowplot")

title4 <- ggdraw() + draw_label("FRAGMENTATION DEGREE V. PMI (DAYS)", fontface='bold')
FG.plot<-plot_grid(pg1,pg2,pg3,pg4,pg5, labels=c("A", "B", "C", "D", "E"), ncol = 2, nrow = 3)
plot_grid(title4, FG.plot, ncol=1, rel_heights=c(0.1,1))
#dev.off()
```

# TBS vs Calculated ADD Megyest et al. & Moffatt et al. 
```{r}
#isolate data used
Pigs.TBS.ADD.all<-select(TBS.ADD.data,Sample,Pig_ID, TBS_Megyesi,TBS_Keough, TBS_Ribereau.Gayon, M_ADD_Megyesi,
                    K_ADD_Megyesi, RB_ADD_Megyesi,M_ADD_Moffatt,
                    K_ADD_Moffatt,RB_ADD_Moffatt)
Pigs.TBS.ADD.all
#Prep
library(ggplot2)

#Build Regression Model for TBS (megyesi) v ADD (megyesi)
reg_model_Meg.ADD_TBS.Megyesi<-lm(TBS_Megyesi~M_ADD_Megyesi, data = Pigs.TBS.ADD.all)
coeff_reg_model_Meg.ADD_TBS.Megyesi<-coefficients(reg_model_Meg.ADD_TBS.Megyesi)
int_reg_model_Meg.ADD_TBS.Megyesi<-coeff_reg_model_Meg.ADD_TBS.Megyesi[1]
slope_reg_model_Meg.ADD_TBS.Megyesi<-coeff_reg_model_Meg.ADD_TBS.Megyesi[2]

#Plot
Plot.x<-ggplot(Pigs.TBS.ADD.all,mapping=aes(x=TBS_Megyesi, y=M_ADD_Megyesi,color=as.factor(Pig_ID) ))+geom_point(position = position_jitter(w=1.3,h=0)) +geom_line(mapping=aes(y=M_ADD_Megyesi))+xlab("Total Body Score (Megyesi et al.)")+ylab("Calculated ADD (Megyesi et al.)")+ scale_color_brewer(palette="BrBG")+ labs(color='Pig ID')

#Build Regression Model for TBS (Keough) v ADD (Megyesi)
reg_model_Meg.ADD_TBS.Keough<-lm(K_ADD_Megyesi~TBS_Keough, data = Pigs.TBS.ADD.all)
coeff_reg_model_Meg.ADD_TBS.Keough<-coefficients(reg_model_Meg.ADD_TBS.Keough)
int_reg_model_Meg.ADD_TBS.Keough<-coeff_reg_model_Meg.ADD_TBS.Keough[1]
slope_reg_model_Meg.ADD_TBS.Keough<-coeff_reg_model_Meg.ADD_TBS.Keough[2]


#Plot
Plot.y<-ggplot(Pigs.TBS.ADD.all,mapping=aes(x=TBS_Keough, y=K_ADD_Megyesi, color=as.factor(Pig_ID)))+geom_line(mapping=aes(y=K_ADD_Megyesi))+geom_point(position = position_jitter(w=1.3,h=0))+xlab("Total Body Score (Keough et al.)")+ylab("Calculated ADD (Megyesi et al.)")+ scale_color_brewer(palette="BrBG")+ labs(color='Pig ID')

#Build Regression Model for TBS (Ribereau.Gayon) v ADD (Megyesi)
reg_model_Meg.ADD_TBS.Ribereau.Gayon<-lm(RB_ADD_Megyesi~TBS_Ribereau.Gayon, data = Pigs.TBS.ADD.all)
coeff_reg_model_Meg.ADD_TBS.Ribereau.Gayon<-coefficients(reg_model_Meg.ADD_TBS.Ribereau.Gayon)
int_reg_model_Meg.ADD_TBS.Ribereau.Gayon<-coeff_reg_model_Meg.ADD_TBS.Ribereau.Gayon[1]
slope_reg_model_Meg.ADD_TBS.Ribereau.Gayon<-coeff_reg_model_Meg.ADD_TBS.Ribereau.Gayon[2]

#Plot
Plot.z<-ggplot(Pigs.TBS.ADD.all,mapping=aes(x=TBS_Ribereau.Gayon, y=RB_ADD_Megyesi, color=as.factor(Pig_ID)))+geom_point(position = position_jitter(w=1.3,h=0))+geom_line(mapping=aes(y=RB_ADD_Megyesi))+xlab("Total Body Score (R.Gayon et al.)")+ylab("Calculated ADD (Megyesi et al.)")+ scale_color_brewer(palette="BrBG")+ labs(color='Pig ID')

#Build Regression Model for Megyesi et. al (TBS) & Moffatt et. al (ADD)
reg_model_Moff.ADD_TBS.Megyesi<-lm(M_ADD_Moffatt~TBS_Megyesi, data = Pigs.TBS.ADD.all)
coeff_reg_model_Moff.ADD_TBS.Megyesi<-coefficients(reg_model_Moff.ADD_TBS.Megyesi)
int_reg_model_Moff.ADD_TBS.Megyesi<-coeff_reg_model_Moff.ADD_TBS.Megyesi[1]
slope_reg_model_Moff.ADD_TBS.Megyesi<-coeff_reg_model_Moff.ADD_TBS.Megyesi[2]


#Plot
Plot.01<-ggplot(Pigs.TBS.ADD.all,mapping=aes(x=TBS_Megyesi, y=M_ADD_Moffatt, color=as.factor(Pig_ID)))+geom_point(position = position_jitter(w=1.3,h=0))+geom_line(mapping=aes(y=M_ADD_Moffatt))+xlab("Total Body Score (Megyesi et al.)")+ylab("Calculated ADD (Moffatt et al.)")+ scale_color_brewer(palette="BrBG")+ labs(color='Pig ID')

#Build Regression Model for Keough et. al (TBS) & Moffatt et. al (ADD)
reg_model_Moff.ADD_TBS.Keough<-lm(K_ADD_Moffatt~TBS_Keough, data = Pigs.TBS.ADD.all)
coeff_reg_model_Moff.ADD_TBS.Keough<-coefficients(reg_model_Moff.ADD_TBS.Keough)
int_reg_model_Moff.ADD_TBS.Keough<-coeff_reg_model_Moff.ADD_TBS.Keough[1]
slope_reg_model_Moff.ADD_TBS.Keough<-coeff_reg_model_Moff.ADD_TBS.Keough[2]


#Plot
Plot.02<-ggplot(Pigs.TBS.ADD.all,mapping=aes(x=TBS_Keough, y=K_ADD_Moffatt, color=as.factor(Pig_ID)))+geom_point(position = position_jitter(w=1.3,h=0))+geom_line(mapping=aes(y=K_ADD_Moffatt))+xlab("Total Body Score (Keough et al.)")+ylab("Calculated ADD(Moffatt et al.)")+ scale_color_brewer(palette="BrBG")+ labs(color='Pig ID')

 
#Build Regression Model for Ribereau Gayon  et. al (TBS) Moffatt et. al (ADD)
reg_model_Moff.ADD_TBS.Ribereau.Gayon<-lm(RB_ADD_Moffatt~TBS_Ribereau.Gayon, data = Pigs.TBS.ADD.all)
coeff_reg_model_Moff.ADD_TBS.Ribereau.Gayon<-coefficients(reg_model_Moff.ADD_TBS.Ribereau.Gayon)
int_reg_model_Moff.ADD_TBS.Ribereau.Gayon<-coeff_reg_model_Moff.ADD_TBS.Ribereau.Gayon[1]
slope_reg_model_Moff.ADD_TBS.Ribereau.Gayon<-coeff_reg_model_Moff.ADD_TBS.Ribereau.Gayon[2]


#Plot
Plot.03<-ggplot(Pigs.TBS.ADD.all,mapping=aes(x=TBS_Ribereau.Gayon, y=RB_ADD_Moffatt, color=as.factor(Pig_ID)))+geom_point(position = position_jitter(w=1.3,h=0))+geom_line(mapping=aes(y=RB_ADD_Moffatt))+xlab("Total Body Score (R. Gayon et al.)")+ylab("Calculated ADD (Moffatt et al.)")+ scale_color_brewer(palette="BrBG")+ labs(color='Pig ID')
```

# TBS vs ADD (Threshold)
```{r}
pdf("Joelle-TBSvADD")
#Build Regression Model for ADD (threshold) v TBS(megyesi)
reg_model_Meg.ADD_TBS.Megyesi<-lm(M_ADD_Megyesi~TBS_Megyesi, data = TBS.ADD.data)
coeff_reg_model_Meg.ADD_TBS.Megyesi<-coefficients(reg_model_Meg.ADD_TBS.Megyesi)
int_reg_model_Meg.ADD_TBS.Megyesi<-coeff_reg_model_Meg.ADD_TBS.Megyesi[1]
slope_reg_model_Meg.ADD_TBS.Megyesi<-coeff_reg_model_Meg.ADD_TBS.Megyesi[2]

#Plot
d<-ggplot(TBS.ADD.data,mapping=aes(x=TBS_Megyesi, y=M_ADD_Megyesi,color=as.factor(Pig_ID) ))+geom_point() +geom_line(mapping=aes(y=M_ADD_Megyesi))+ylab("Calculated ADD (Temperature)")+xlab("Total Body Score (Megyesi et al.)")+ scale_color_brewer(palette="BrBG")+ labs(color='Pig ID')

#Build Regression Model for ADD (threshold) v ADD (Keough)
reg_model_Meg.ADD_TBS.Keough<-lm(K_ADD_Megyesi~TBS_Keough, data = TBS.ADD.data)
coeff_reg_model_Meg.ADD_TBS.Keough<-coefficients(reg_model_Meg.ADD_TBS.Keough)
int_reg_model_Meg.ADD_TBS.Keough<-coeff_reg_model_Meg.ADD_TBS.Keough[1]
slope_reg_model_Meg.ADD_TBS.Megyesi<-coeff_reg_model_Meg.ADD_TBS.Keough[2]

#Plot
e<-ggplot(TBS.ADD.data,mapping=aes(x=TBS_Keough, y=K_ADD_Megyesi,color=as.factor(Pig_ID) ))+geom_point() +geom_line(mapping=aes(y=K_ADD_Megyesi))+ylab("Calculated ADD (Temperature)")+xlab("Total Body Score (Keough et al.)")+ scale_color_brewer(palette="BrBG")+ labs(color='Pig ID')

#Build Regression Model for ADD (threshold) v ADD (R.Gayon)
reg_model_Meg.ADD_TBS.Ribereau.Gayon<-lm(RB_ADD_Megyesi~TBS_Ribereau.Gayon, data = TBS.ADD.data)
coeff_reg_model_Meg.ADD_TBS.Ribereau.Gayon<-coefficients(reg_model_Meg.ADD_TBS.Ribereau.Gayon)
int_reg_model_Meg.ADD_TBS.Ribereau.Gayon<-coeff_reg_model_Meg.ADD_TBS.Ribereau.Gayon[1]
slope_reg_model_Meg.ADD_TBS.Ribereau.Gayon<-coeff_reg_model_Meg.ADD_TBS.Ribereau.Gayon[2]

#Plot
f<-ggplot(TBS.ADD.data,mapping=aes(x=TBS_Ribereau.Gayon, y=RB_ADD_Megyesi,color=as.factor(Pig_ID) ))+geom_point() +geom_line(mapping=aes(y=RB_ADD_Megyesi))+ylab("Calculated ADD (Temperature)")+xlab("Total Body Score (R. Gayon et al.)")+ scale_color_brewer(palette="BrBG")+ labs(color='Pig ID')

#put all plots together in one figure
install.packages("cowplot")
library("cowplot")
#put all plots together in one figure

title.2 <- ggdraw() + draw_label("CALCULATED ADD METHODS V. TBS FROM TBS-SYSTEMS", fontface='bold')
plot.grid1<-plot_grid(Plot.x, Plot.y, Plot.z,Plot.01, Plot.02, Plot.03, d, e, f, labels=c("A", "B", "C", "D", "E", "F", "G", "H", "I"), ncol = 3, nrow = 3)
plot_grid(title.2, plot.grid1, ncol=1, rel_heights=c(0.1,1))
#dev.off
```

# ADD versus ADD
```{r}
pdf('Joelle-ADDvADD.pdf')
#Build Regression Model for ADD (threshold) v ADD (megyesi) --> megyesi et al. equation 
reg_model_Meg.ADD_TBS.Megyesi<-lm(M_ADD_Megyesi~Thres_ADD.minus.five, data = TBS.ADD.data)
coeff_reg_model_Meg.ADD_TBS.Megyesi<-coefficients(reg_model_Meg.ADD_TBS.Megyesi)
int_reg_model_Meg.ADD_TBS.Megyesi<-coeff_reg_model_Meg.ADD_TBS.Megyesi[1]
slope_reg_model_Meg.ADD_TBS.Megyesi<-coeff_reg_model_Meg.ADD_TBS.Megyesi[2]


#Plot
a<-ggplot(TBS.ADD.data,mapping=aes(x=Thres_ADD.minus.five, y=M_ADD_Megyesi,color=as.factor(Pig_ID) ))+geom_point(position = position_jitter(w=1.9,h=0))+geom_line(mapping=aes(y=M_ADD_Megyesi))+xlab("Calculated ADD (Megyesi et al.) using TBS ")+ylab("Temp. Calculated ADD")+ scale_color_brewer(palette="BrBG")+ labs(color='Pig ID')

#Build Regression Model for ADD (threshold) v ADD (Keough) --> megyesi et al. equation 
reg_model_Meg.ADD_TBS.Keough<-lm(K_ADD_Megyesi~Thres_ADD.minus.five, data = TBS.ADD.data)
coeff_reg_model_Meg.ADD_TBS.Keough<-coefficients(reg_model_Meg.ADD_TBS.Keough)
int_reg_model_Meg.ADD_TBS.Keough<-coeff_reg_model_Meg.ADD_TBS.Keough[1]
slope_reg_model_Meg.ADD_TBS.Megyesi<-coeff_reg_model_Meg.ADD_TBS.Keough[2]

#Plot
b<-ggplot(TBS.ADD.data,mapping=aes(x=Thres_ADD.minus.five, y=K_ADD_Megyesi,color=as.factor(Pig_ID) ))+geom_point(position = position_jitter(w=1.9,h=0)) +geom_line(mapping=aes(y=K_ADD_Megyesi))+xlab("Calculated ADD (Megyesi et al.) using TBS (Keough et al.)")+ylab("Temp. Calculated ADD")+ scale_color_brewer(palette="BrBG")+ labs(color='Pig ID')

#Build Regression Model for ADD (threshold) v ADD (R.Gayon) --> megyesi et al. equation 
reg_model_Meg.ADD_TBS.Ribereau.Gayon<-lm(RB_ADD_Megyesi~Thres_ADD.minus.five, data = TBS.ADD.data)
coeff_reg_model_Meg.ADD_TBS.Ribereau.Gayon<-coefficients(reg_model_Meg.ADD_TBS.Ribereau.Gayon)
int_reg_model_Meg.ADD_TBS.Ribereau.Gayon<-coeff_reg_model_Meg.ADD_TBS.Ribereau.Gayon[1]
slope_reg_model_Meg.ADD_TBS.Ribereau.Gayon<-coeff_reg_model_Meg.ADD_TBS.Ribereau.Gayon[2]

#Plot
c<-ggplot(TBS.ADD.data,mapping=aes(x=Thres_ADD.minus.five, y=RB_ADD_Megyesi,color=as.factor(Pig_ID) ))+geom_point(position = position_jitter(w=1.9,h=0)) +geom_line(mapping=aes(y=RB_ADD_Megyesi))+xlab("Calculated ADD (Megyesi et al.) using TBS (R.Gayon et al.)")+ylab("Temp. Calculated ADD")+ scale_color_brewer(palette="BrBG")+ labs(color='Pig ID')

#Build Regression Model for ADD (threshold) v ADD (megyesi) --> moffatt et al. equation 
reg_model_Moff.ADD_TBS.Megyesi<-lm(M_ADD_Moffatt~Thres_ADD.minus.five, data = TBS.ADD.data)
coeff_reg_model_Moff.ADD_TBS.Megyesi<-coefficients(reg_model_Moff.ADD_TBS.Megyesi)
int_reg_model_Moff.ADD_TBS.Megyesi<-coeff_reg_model_Moff.ADD_TBS.Megyesi[1]
slope_reg_model_Moff.ADD_TBS.Megyesi<-coeff_reg_model_Moff.ADD_TBS.Megyesi[2]

#Plot
plt1<-ggplot(TBS.ADD.data,mapping=aes(x=Thres_ADD.minus.five, y=M_ADD_Moffatt,color=as.factor(Pig_ID) ))+geom_point(position = position_jitter(w=1.9,h=0)) +geom_line(mapping=aes(y=M_ADD_Moffatt))+xlab("Calculated ADD (Moffatt et al.) using TBS (Megyesi et al.)")+ylab("Temp. Calculated ADD")+ scale_color_brewer(palette="BrBG")+ labs(color='Pig ID')

#Build Regression Model for ADD (threshold) v ADD (Keough) --> moffatt et al. equation
reg_model_Moff.ADD_TBS.Keough<-lm(K_ADD_Moffatt~Thres_ADD.minus.five, data = TBS.ADD.data)
coeff_reg_model_Moff.ADD_TBS.Keough<-coefficients(reg_model_Moff.ADD_TBS.Keough)
int_reg_model_Moff.ADD_TBS.Keough<-coeff_reg_model_Moff.ADD_TBS.Keough[1]
slope_reg_model_Moff.ADD_TBS.Megyesi<-coeff_reg_model_Moff.ADD_TBS.Keough[2]

#Plot
plt2<-ggplot(TBS.ADD.data,mapping=aes(x=Thres_ADD.minus.five, y=K_ADD_Moffatt,color=as.factor(Pig_ID) ))+geom_point(position = position_jitter(w=1.9,h=0))+geom_line(mapping=aes(y=K_ADD_Moffatt))+xlab("Calculated ADD (Moffatt et al.) using TBS (Keough et al.)")+ylab("Temp. Calculated ADD")+ scale_color_brewer(palette="BrBG")+ labs(color='Pig ID')

#Build Regression Model for ADD (threshold) v ADD (R.Gayon) --> moffatt et al. equation
reg_model_Meg.ADD_TBS.Ribereau.Gayon<-lm(RB_ADD_Moffatt~Thres_ADD.minus.five, data = TBS.ADD.data)
coeff_reg_model_Meg.ADD_TBS.Ribereau.Gayon<-coefficients(reg_model_Meg.ADD_TBS.Ribereau.Gayon)
int_reg_model_Meg.ADD_TBS.Ribereau.Gayon<-coeff_reg_model_Meg.ADD_TBS.Ribereau.Gayon[1]
slope_reg_model_Meg.ADD_TBS.Ribereau.Gayon<-coeff_reg_model_Meg.ADD_TBS.Ribereau.Gayon[2]

#Plot
plt3<-ggplot(TBS.ADD.data,mapping=aes(x=Thres_ADD.minus.five, y=RB_ADD_Moffatt,color=as.factor(Pig_ID) ))+geom_point(position = position_jitter(w=1.9,h=0)) +geom_line(mapping=aes(y=RB_ADD_Moffatt))+xlab("Calculated ADD (Moffatt et al.) using TBS (R.Gayon et al.)")+ylab("Temp. Calculated ADD")+ scale_color_brewer(palette="BrBG")+ labs(color='Pig ID')

#put all plots together in one figure
install.packages("cowplot")
library("cowplot")

#put all plots together in one figure
title3 <- ggdraw() + draw_label("CALCULATED ADD USING TEMPERATURE V. CALCULATED ADD USING TBS", fontface='bold')
plot.Grid<-plot_grid(a, b, c,plt1, plt2, plt3, labels=c("A", "B", "C", "D", "E", "F"), ncol = 3, nrow = 3)
plot_grid(title3, plot.Grid, ncol=1, rel_heights=c(0.1,1))

#dev.off
```
#Making Datatable for slope, R-squared, and p-values for ALL data
```{r}
#For the ALL TBS and ADD data

pdf("Joelle.Tables.pdf")
install.packages("kableExtra")
library(kableExtra)

dt <-data.frame('Model' = c("TBS v. PMI", "TBS v. ADD (Megyesi et al.)", "TBS v. ADD (Moffatt et al.)", "TBS v. ADD (THD)", "ADD (THD) v. ADD (Megyesi et al.)", "ADD (THD) v. ADD (Moffatt et al.)"),
      "Slope_1"= c(0.436, 0.254,4.876,0.254,0.148,4.876),
      'Intercept_1' = c(0.0252,'-14.692',45.218, '-14.792', 0.254,45.218 ),
      'P_value_1' = c('1.263e-11','2.2e-16','2.2e-16','2.2e-16','2.2e-16','2.2e-16'),
      'R_Squared_1' = c(0.752,0.852,0.894,0.852,0.852,0.894),  Slope_2 = c(0.363,3.307,3.307,4.824,3.307,4.824),
      'Intercept_2' = c(0.025,60.827,45.944,60.827,60.827,45.044),
      'P_value_2' = c('1.687e-12','2.2e-16','2.2e-16','2.2e-16','2.2e-16','2.2e-16'),
      'R_Squared_2' = c(0.776,0.863,0.903,0.860,0.860,0.903),  Slope_3 = c(0.215,3.307,5.293,3.706,3.706,3.706),
      'Intercept_3' = c(0.025,59.825,43.874,59.825,59.825,59.825),
      'P_value_3' = c('2.2e-16','2.2e-16','2.2e-16','2.2e-16','2.2e-16','2.2e-16'),
      'R_Squared_3' = c(0.900, 0.837,0.875,0.834,0.834,0.837))
kbl(dt) %>%
  kable_classic_2(full_width=F, html_font = "Cambria", ) %>%
  add_header_above(c(" " =1, "Megyesi et al." = 4, "Keough et al." =4, "Ribéreau-Gayon et al." = 4))%>%
  kable_paper("striped", full_width=F)

#For DNA Data
DT <-data.frame('Region' = c("500-1000", "1000-5000", "5000-10000", "10000-50000", ""),
      "Slope"= c('-0.427','-7.173','-0.309','-23.30','-0.053' ),
      'Intercept' = c(767.699, 2797.541,7042.226,21141.90,7.073 ),
      'P_value' = c('6.64e-5','2.193e-7',0.187,'0.110','2.64e-9'), 'R_Squared' = c(0.523,0.693,0.121,0.156,0.781))

kbl(DT) %>%
  kable_classic_2(full_width=F, html_font = "Cambria", ) %>%
  add_header_above(c(" " =1, "Fragmentation Degree v PMI" = 4))%>%
  kable_paper("striped", full_width=F) %>%
  pack_rows("DIN", 5, 5, color = "Black", background = "azure4")
dev.off
```

