#Export the sample names, quantities, and amp status from the results of the QS3 software.
#Manually look at the data in excel and remove weird data.
#Set all NAs (blank cells) for "no amp" to 0 and recalculate the average PCR replicates.
#For success_data, format it into a tab-delim .txt doc with sample in column 1 and quantity in column 2.
#For success_bar, replace quantities in success_data with "success" if q>0, and "failure" if q=0.
#For boxplot_data (quantities per sample), format it into a tab-delim .txt doc with sample in column 1 and quantity in column 2.

#%success
suc <- read.delim("success_data.txt")
#subset
Suc_bl <- suc[c(1:3),c(1:2)]
Suc_pos <- suc[c(4:33),c(1:2)]
Suc_neg <- suc[c(34:41),c(1:2)]
Suc_pa <- suc[c(42:76),c(1:2)]
Suc_pr <- suc[c(77:105),c(1:2)]
Suc_f <- suc[c(106:134),c(1:2)]
#total qPCR reps
length(which(suc$sample=="+blood"))
length(which(suc$sample=="+denim"))
length(which(suc$sample=="-denim"))
length(which(suc$sample=="passive"))
length(which(suc$sample=="pressure"))
length(which(suc$sample=="friction"))
#number of successes per sample
length(which(Suc_bl$Quantity!=0))
length(which(Suc_pos$Quantity!=0))
length(which(Suc_neg$Quantity!=0))
length(which(Suc_pa$Quantity!=0))
length(which(Suc_pr$Quantity!=0))
length(which(Suc_f$Quantity!=0))
#number of failures per sample
length(which(Suc_bl$Quantity==0))
length(which(Suc_pos$Quantity==0))
length(which(Suc_neg$Quantity==0))
length(which(Suc_pa$Quantity==0))
length(which(Suc_pr$Quantity==0))
length(which(Suc_f$Quantity==0))
#mean and sd per sample
suc_mean <- aggregate(suc$Quantity, by = list(suc$sample),mean)
suc_sd <- aggregate(suc$Quantity, by = list(suc$sample),sd)
#put all of these into a tab delim table called success_table.txt

#replace quants >0 with "success" and quants=0 to "failure" in a new .txt file.
suc_bar <- read.delim("suc_barplot_data.txt")
#barplot
suc_bar$sample2<-factor(suc_bar$sample,levels=levels(suc_bar$sample)[c(2,3,1,5,6,4)]) #reorder samples so bars are in right order
counts <- table(suc_bar$Success, suc_bar$sample2)
suc_plot <- barplot(counts,
                    xlab="Sample",
                    ylab = "Number of qPCR Replicates",
                    ylim = c(0,50),
                    names.arg = c("+ Control\nBlood", "+ Control\nDenim", "- Control\nDenim", "Passive", "Pressure", "Friction"),
                    col=c("red", "darkblue"),
                    legend.text = rownames(counts),
                    args.legend = list(x="topleft"))

#transfer/no_transfer, p-value <0.05 means there is a sig diff in successful transfers between the types of contact
suc_table <- read.delim("success_table.txt")
#subset
fish <- suc_table[c(4:6), c(3,4)]
fisher.test(fish)

#boxplot of quantities http://www.sthda.com/english/wiki/kruskal-wallis-test-in-r
setwd("~/5th (Fifth) Year/Mirror/thesis/notes")
quants <- read.delim("boxplot_data3.txt")
attach(quants)
library(ggpubr)
g1 <- ggboxplot(quants, x = "sample", y = "quantity", 
                color = "sample", palette = c("#00AFBB", "#E7B800", "#FC4E07", "#009900", "#660066", "#cc0000"),
                order = c("+ control blood", "+ control denim", "- control denim", "passive", "pressure", "friction"),
                ylab = "Quantity (ng/uL)", xlab = "Sample")

full_boxplot <- g1 +
  scale_x_discrete(labels=c("+ Control\nBlood", "+ Control\nDenim", "- Control\nDenim", "Passive", "Pressure", "Friction"))+
  theme(legend.position = "none") +
  geom_jitter(aes(color=sample))

#zoom into -denim to friction
sub_quants <- quants[c(12:52),c(1:2)]
g2 <- ggboxplot(sub_quants, x = "sample", y = "quantity", 
                color = "sample", palette = c("#FC4E07", "#009900", "#660066", "#cc0000"),
                order = c("- control denim", "passive", "pressure", "friction"),
                ylab = "Quantity (ng/uL)", xlab = "Sample")
subset_boxplot <- g2 +
  scale_x_discrete(labels=c("- Control\nDenim", "Passive", "Pressure", "Friction"))+
  theme(legend.position = "none")+
  geom_jitter(aes(color=sample))

plot(full_boxplot)
plot(subset_boxplot)

#calculate %transfer from the quantities and add it as a 3rd column to quants
mean_quants <- aggregate(quants$quantity, by = list(quants$sample),mean)
sd_quants <- aggregate(quants$quantity, by = list(quants$sample),sd)
quants$percent_transfer <- (quants$quantity)/mean_quants[3,2]*100
#subset just the %transfers
per_trans <- quants[c(20:52),c(1,3)]
attach(per_trans)
#subset for each sample to test normality
pa <- per_trans[c(1:13),c(1:2)]
pr <- per_trans[c(14:23),c(1:2)]
f <- per_trans[c(24:33),c(1:2)]
#is my data normally distributed?
#shapiro-wilkes test of significance
shapiro.test(pa$percent_transfer) #a p-value of <0.05 means it is not normal (sig diff from a normal distrubution)
shapiro.test(pr$percent_transfer)
shapiro.test(f$percent_transfer)
#graph to see if it looks like a normal distribution
library(ggpubr)
ggdensity(pa$percent_transfer, xlab = "% transfer", main = "passive") #does it look normal on a graph?
ggdensity(pr$percent_transfer, xlab = "% transfer", main = "pressure")
ggdensity(f$percent_transfer, xlab = "% transfer", main = "friction")

#KW test: if p-value is less than the sig level of 0.05, reject H0, yes sig diff
kruskal.test(percent_transfer ~ sample, data = per_trans)

#Mann-Whitney_Wilcox test
#subset just the %transfer for each type of contact
pass <- per_trans[c(1:13),c(2)]
pres <- per_trans[c(14:23),c(2)]
fric <- per_trans[c(24:33),c(2)]
#compare each type of contact to see which ones are sig diff
#if p-value is less than the sig level of 0.05, reject H0, yes sig diff
wilcox.test(pass, pres, alternative = "two.sided")
wilcox.test(pres, fric, alternative = "two.sided")
wilcox.test(pass, fric, alternative = "two.sided")

#%transfer summary stats
mean_pt <- aggregate(per_trans$percent_transfer, by = list(per_trans$sample),mean)
sd_pt <- aggregate(per_trans$percent_transfer, by = list(per_trans$sample),sd)
library(plyr)
count(per_trans, "sample")

#power analysis
#power to analyze quantities
install.packages("wmwpow")
library(wmwpow)
#pos control vs passive (w mean_quants)
wmwpowd(n = 10, m = 13, distn = "norm(0.1344200, 0.1031181)", 
        distm = "norm(0.00008759231, 0.0001457375)",
        sides = "two.sided",
        alpha = 0.05, nsims=1000)
#pass vs pres
wmwpowd(n = 13, m = 10, distn = "norm(0.00008759231, 0.0001457375)", 
        distm = "norm(0.00006847000, 0.00006245651)",
        sides = "two.sided",
        alpha = 0.05, nsims=1000)
#pass vs fric
wmwpowd(n = 13, m = 10, distn = "norm(0.00008759231, 0.0001457375)", 
        distm = "norm(0.0003498500, 0.0004122668)",
        sides = "two.sided",
        alpha = 0.05, nsims=1000)
#neg vs pass
wmwpowd(n = 8, m = 13, distn = "norm(0.000009287500, 0.00002626902)", 
        distm = "norm(0.00008759231, 0.0001457375)",
        sides = "two.sided",
        alpha = 0.05, nsims=1000)
#pos vs fric
wmwpowd(n = 10, m = 10, distn = "norm(0.1344200, 0.1031181)", 
        distm = "norm(0.0003498500, 0.0004122668)",
        sides = "two.sided",
        alpha = 0.05, nsims=1000)
#neg vs fric
wmwpowd(n = 8, m = 10, distn = "norm(0.000009287500, 0.00002626902)", 
        distm = "norm(0.0003498500, 0.0004122668)",
        sides = "two.sided",
        alpha = 0.05, nsims=1000)
#neg vs press
wmwpowd(n = 8, m = 10, distn = "norm(0.000009287500, 0.00002626902)", 
        distm = "norm(0.00006847000, 0.00006245651)",
        sides = "two.sided",
        alpha = 0.05, nsims=1000)
#press vs fric
wmwpowd(n = 10, m = 10, distn = "norm(0.00006847000, 0.00006245651)", 
        distm = "norm(0.0003498500, 0.0004122668)",
        sides = "two.sided",
        alpha = 0.05, nsims=1000)
#power to analyze success
install.packages("statmod")
library(statmod)
#pos vs pass
power.fisher.test(1.0, 0.4, 30, 35, alpha=0.05, nsim=1000)
#pos vs fric
power.fisher.test(1.0, 0.97, 30, 29, alpha=0.05, nsim=1000)
#pass vs pres
power.fisher.test(0.4, 0.76, 35, 29, alpha=0.05, nsim=1000)
#pass vs fric
power.fisher.test(0.4, 0.97, 35, 29, alpha=0.05, nsim=1000)
#neg vs pass
power.fisher.test(0.13, 0.4, 8, 35, alpha=0.05, nsim=1000)
#neg vs fric
power.fisher.test(0.13, 0.97, 8, 29, alpha=0.05, nsim=1000)
#neg vs pres
power.fisher.test(0.13, 0.76, 8, 29, alpha=0.05, nsim=1000)
#pres vs fric
power.fisher.test(0.76, 0.97, 29, 29, alpha=0.05, nsim=1000)
