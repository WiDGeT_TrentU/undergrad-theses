#!/bin/bash
#SBATCH --mem=50G
#SBATCH --time=0-16:00           # time (DD-HH:MM)
#SBATCH --account=rrg-shaferab
#SBATCH --mail-user=shanewidanagama@trentu.ca
#SBATCH --mail-type=END
#SBATCH --mail-type=FAIL

module load mummer/3.23

#align various chloroplast (Typha latifolia) and mitochondrial genomes (zea mays, oryza sativa) of related species to the reference T. latifolia genome
nucmer -p aln zmays_mito.fasta ../Pilon/t1/t_latifolia_polished.fasta

#find alignments longer than 5000 bp by running show-coords:
#e.g. show-coords tlat_aln.delta
#and  run 1000 bp subsequences of those alignments using ncbi blast

#The following is an example of how individual scaffolds were extracted
#trim the headers to only include the scaffold name
#cut -d '_' -f1 t_latifolia_polished.fasta > trimTLatPolish.fasta
#extract tig00001508 with contigToExtract.list containing the name tig00001508
#seqtk subseq trimTLatPolish.fasta contigToExtract.list > tig00001508
