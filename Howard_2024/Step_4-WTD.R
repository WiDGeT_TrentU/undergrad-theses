#White-tailed deer

library(dplyr)
library(ggplot2)
library(gghighlight)
library(ggrepel)
library(readr)

#set working directory desktop
setwd("~/Desktop/Thesis R")

#load annotation data from 1.1_Probe_alignment script
Anno_SC <- readRDS("~/Desktop/Thesis R/Human.Homo_sapiens.hg19.Amin.V9.RDS")


#load EWAS data from 4.1_EWAS_Island_effect script
tab <- read_csv("EbFit_AllDeer_effect2.csv")

#merge EWAS and annotations based on CGid
allData <- merge(tab,Anno_SC, by = "CGid")

#make numeric variables for base pair (where probe starts) and chromosome (scaffold name)
allData$bp <- as.numeric(allData$CGstart)

#make column with cumulative base pairs
#selects the largest position for each chromosome, and then calculates the cumulative sum of those
data_cum <- allData %>% 
  group_by(geneChr) %>% 
  summarise(max_bp = max(bp)) %>% 
  mutate(bp_add = lag(cumsum(max_bp), default = 0)) %>% 
  select(geneChr, bp_add)

#add to main dataset the new column
allData_manhattan <- allData %>% 
  inner_join(data_cum, by = "geneChr") %>% 
  mutate(bp_cum = bp + bp_add)

#rename p value column
allData_manhattan$p <- allData_manhattan$p.value.CWDpositive
#based on t value ( - or +) make new column indicating hyper or hypomethylated
allData_manhattan <- allData_manhattan %>%
  mutate(direction = case_when(allData_manhattan$t.CWD....positive < 0 ~ "neg", allData_manhattan$t.CWD....positive > 0 ~ "pos"))
#save to computer
write.csv(allData_manhattan,file="allData_manhattan_CWD_Deer2.csv")


#to set axis ticks to be in middle of each chromosome
axis_set <- allData_manhattan %>% 
  group_by(geneChr) %>% 
  summarize(center = mean(bp_cum))


#indicate the significance threshold
#bonferroni (0.05/24460 total obs)
sig <- 2e-6

#make subsetted data frames for top hypo and hypermethylated sites (for plotting different colors)
significant<- allData_manhattan %>% filter(p.value.CWD....positive < sig)
significant_pos <- subset(significant, direction =="pos")
significant_neg <- subset(significant, direction =="neg")

#ggplot for Manhattan plot
#x axis is location of CpGs on genome, y is the log of p value, want each scaffold to have alternating color
manhplot <- ggplot(allData_manhattan, aes(x = bp_cum, y = -log10(p.value.CWD....positive))) +
  #line for significant p value threshold (2e-6), make it red and dashed
  geom_hline(yintercept = -log10(sig), color = "#f32712", linetype = "dashed") +
  #color each point based on scaffold and change size
  geom_point(aes(color=as.factor(geneChr)), size=0.8) +
  #point color for significant + (red) and - (blue) methylated genes
  #make bigger than non significant points
  geom_point(data = significant_pos, colour = "#fb6869" ,size=1.2) +
  geom_point(data = significant_neg, colour = "#66b1ff",size=1.2) +
  #annotation info for top n (want 15 but many don't have gene annotations so select 27 to get 15)
  #add label for gene name, make font size 5 and black
  geom_text_repel(data=allData_manhattan %>% top_n(-10, p.value.CWD....positive), aes(label=SYMBOL), size=5, color ="black") +
  #place ticks in middle of each chromosome/scaffold on x axis
  scale_x_continuous(label = axis_set$geneChr, breaks = axis_set$center) +
  #y axis limits set and breaks based on p values
  #start at 0 till 24 but labels start at 4 till 20 with jumps of 4
  scale_y_continuous(expand = c(0,0), breaks = seq(4, 20, by = 4)) +
  #select colors for non significant plot points
  #make the colors repeat over whole genome
  scale_color_manual(values = rep(c("#bebebe", "#f8e9d5"), unique(length(axis_set$geneChr)))) +
  #axis labels
  labs(x = "Chromosome",y = "-log10(P)") + 
  #title 
  ggtitle("EWAS of CWD in White-Tailed Deer") +
  #setting up background lines and white background color 
  theme(panel.background = element_rect(fill = "white", colour = "white",size = 0.7, linetype = "solid"),
        panel.grid.major = element_line(size = 0.25, linetype = 'solid',colour = "#ececec"), 
        panel.grid.minor = element_line(size = 0.15, linetype = 'solid',colour = "#ececec"),
        #remove legend
        legend.position = "none",
        #remove vertical lines and chromosome numbers, basically just make the plot cleaner
        panel.border = element_blank(),
        panel.grid.major.x = element_blank(),
        panel.grid.minor.x = element_blank(),
        axis.text.x = element_blank(),
        #position title in the middle and select size and bold
        plot.title = element_text(hjust = 0.5, face="bold")
  )
#plot
plot(manhplot)

#view data for top 57 (there are 56 sig) significant (p value) CpGs
#make new column with what species the gene that match came from
allData_manhattan$species <- allData_manhattan$V6$X3$X2
#select top 30 based on p value
top <- allData_manhattan %>% top_n(-57, p.value.CWD....positive)
#only subset columns of interest
#top_clean = subset(top, select = c("CGid", "geneId", "annotation", "trascriptId"))

