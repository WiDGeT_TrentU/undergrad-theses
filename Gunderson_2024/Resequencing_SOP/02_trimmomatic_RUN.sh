# TruSeq3-PE.fa must be in working directory

for f in $(ls *.fastq.gz | cut -f1-4 -d'_'| uniq)
do
echo $f
job=02-trim-$f
sbatch -o $out/$job.out --job-name=$job $script/02_trimmomatic.sh ${f} ${trim}
sleep 10
done