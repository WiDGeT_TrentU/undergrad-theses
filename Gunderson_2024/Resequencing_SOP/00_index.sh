#!/bin/bash
#SBATCH --time=0-16:00
#SBATCH --account=rrg-shaferab
#SBATCH --mem=8000M
#SBATCH --job-name=00-index
#SBATCH --output=00-index.out

# 1 is the reference genome prefix

# launch
module load StdEnv/2023
module load bwa
module load picard
module load samtools

java -Xmx8G -jar $EBROOTPICARD/picard.jar CreateSequenceDictionary -R ${1}.fna -O ${1}.dict
samtools faidx ${1}.fna
bwa index ${1}.fna