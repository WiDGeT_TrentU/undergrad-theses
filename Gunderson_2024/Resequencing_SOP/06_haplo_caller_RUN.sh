for b in $(ls *.unique_reads.bam | sed 's/.unique_reads.bam//' | sort -u)
do
echo $b
sample=$(echo $b | sed 's/-OVirginianus_MT//' | sort -u)
job=06-haplotype-$sample
sbatch -o $out/$job.out --job-name=$job $script/06_haplotype.sh ${b} ${g} ${snp}
sleep 10
done 