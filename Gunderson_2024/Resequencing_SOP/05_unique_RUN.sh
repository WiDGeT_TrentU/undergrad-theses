for b in $(ls *.deduped_reads.bam | sed 's/.deduped_reads.bam//' | sort -u)
do
echo $b
sample=$(echo $b | sed 's/-OVirginianus_MT//' | sort -u)
job=05-unique-$sample
sbatch -o $out/$job.out --job-name=$job $script/05_unique.sh ${b} ${soft}
sleep 10
done