#!/bin/bash
#SBATCH --account=rrg-shaferab
#SBATCH --cpus-per-task=30
#SBATCH --mem=32000M
#SBATCH --time=0-18:00 # time (DD-HH:MM)

# 1 is the sample prefix
# 2 is the reference genome
# 3 is the path to output

# launch
module load StdEnv/2023 gatk/4.4

gatk --java-options "-Xmx8G" HaplotypeCaller -R ${2} -I ${1}.unique_reads.bam -O ${3}/${1}.vcf -ERC GVCF