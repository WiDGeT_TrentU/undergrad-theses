#!/bin/bash
#SBATCH --account=rrg-shaferab
#SBATCH --cpus-per-task=16
#SBATCH --mem=64000M

# 1 is the input prefix
# 2 is the reference genome
# 3 is the path to output 
# 4 is the output prefix

module load bwa
module load samtools

bwa mem -M -t 16 -R "@RG\\tID:${1}\\tSM:${1}\\tLB:${1}\\tPL:ILLUMINA" \
${2} ${1}_trim_R1_001.fastq.gz ${1}_trim_R2_001.fastq.gz | \
samtools sort -@ 16 -o ${3}/${4}.sorted_reads.bam &&
samtools flagstat ${3}/${4}.sorted_reads.bam > ${3}/${4}.sorted_reads.flagstat