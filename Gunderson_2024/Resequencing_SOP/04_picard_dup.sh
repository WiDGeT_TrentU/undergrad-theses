#!/bin/bash
#SBATCH --account=rrg-shaferab
#SBATCH --cpus-per-task=30
#SBATCH --mem=99000M
#SBATCH --time=0-04:00 # time (DD-HH:MM)

# download & install sambamba-1.0.1-linux-amd64-static

# 1 is the sample prefix
# 2 is the path to sambamba 

#launch
module load picard
module load samtools

java -Xmx120G -jar $EBROOTPICARD/picard.jar MarkDuplicates \
INPUT=${1}.sorted_reads.bam \
OUTPUT=${1}.deduped_reads.bam \
USE_JDK_DEFLATER=true USE_JDK_INFLATER=true \
ASSUME_SORT_ORDER=coordinate \
REMOVE_DUPLICATES=true REMOVE_SEQUENCING_DUPLICATES=true \
METRICS_FILE=${1}.deduped.picard &&
${2}/sambamba-1.0.1-linux-amd64-static flagstat \
--nthreads=30 \
${1}.deduped_reads.bam \
> ${1}.deduped_reads.flagstat