for b in $(ls *.sorted_reads.bam | sed 's/.sorted_reads.bam//' | sort -u)
do
echo $b
sample=$(echo $b | sed 's/-OVirginianus_MT//' | sort -u)
job=04-picard-dup-$sample
sbatch -o $out/$job.out --job-name=$job $script/04_picard_dup.sh ${b} ${soft}
sleep 10
done