#!/bin/bash
#SBATCH --account=rrg-shaferab
#SBATCH --cpus-per-task=30
#SBATCH --mem=12000M
#SBATCH --time=0-01:00 # time (DD-HH:MM)

# first download & install sambamba-1.0.1-linux-amd64-static

# 1 is the sample prefix
# 2 is the path to software 

#launch
module load samtools

${2}/sambamba-1.0.1-linux-amd64-static view \
--nthreads=30 --with-header --format=bam --show-progress \
-F "mapping_quality >= 1 and not (unmapped or secondary_alignment) and not ([XA] != null or [SA] != null)" \
${1}.deduped_reads.bam \
-o ${1}.unique_reads.bam &&
${2}/sambamba-1.0.1-linux-amd64-static flagstat \
--nthreads=30 \
${1}.unique_reads.bam \
> ${1}.unique_reads.flagstat