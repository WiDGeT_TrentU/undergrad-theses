for f in $(ls *.fastq.gz | cut -f1-4 -d'_'| uniq)
do
echo $f
job=03-bwa-sort-$f
outpref=$f-$sp # change output prefix so filename includes name of species/genome
sbatch -o $out/$job.out --job-name=$job --time=0-04:00 $script/03_BWA_sort.sh ${f} ${g} ${map} ${outpref}
sleep 10
done