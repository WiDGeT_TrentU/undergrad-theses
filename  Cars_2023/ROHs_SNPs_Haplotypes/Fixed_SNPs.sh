#!/bin/bash
#SBATCH --job-name=diagnostic
#SBATCH -A rrg-shaferab
#SBATCH -N 1
#SBATCH --cpus-per-task 32
#SBATCH --mem=0
#SBATCH -t 0-04:59:00
#SBATCH -o %x-%j.log

module load r
module load vcftools
export PATH=$PATH:$(pwd)

DiagnoSNPs.sh -v N90.WTD.51.angsd.phased.vcf -a KD.txt -b SM.txt

---------------------------------------------------------------------------------------------------------

#DiagnoSNPs.sh
#!/bin/bash
version=1.1

usage() {
echo "DiagnoSNPs.sh v $version by Luis Rodrigo Arce-Valdés (04/04/2021)
DiagnoSNPs.sh - A small pipeline to identify SNPs fixed with alternative alleles between two groups of samples

Usage:
DiagnoSNPs.sh -v vcf -a first_group.txt -b second_group.txt -o dir -p prefix

Options:
-v: path to the vcf input file.
-a: path to a text file with the samples names of the first group (Format is 'SAMPLE1\n SAMPLE2\n ...')
-b: path to a text file with the samples names of the second group (Format is 'SAMPLE1\n SAMPLE2\n ...')
-o: path to a directory where to write the output file
-p: prefix for output file

Dependencies:
+ vcftools:
Petr Danecek, Adam Auton, Goncalo Abecasis, Cornelis A. Albers, Eric Banks, Mark A. DePristo, Robert E. Handsaker, Gerton Lunter, Gabor T. Marth, Stephen T. Sherry, Gilean McVean, Richard Durbin, 1000 Genomes Project Analysis Group, The variant call format and VCFtools, Bioinformatics, Volume 27, Issue 15, 1 August 2011, Pages 2156–2158, https://doi.org/10.1093/bioinformatics/btr330

+ R:
R Core Team (2020). R: A language and environment for statistical computing. R Foundation for Statistical Computing, Vienna, Austria. URL https://www.R-project.org/.

You need to have installed both vcftools and R in your OS for DiagnoSNPs.sh to work. If you use this script you must cite on your paper both vcftools and R software.

Output:
The outout of this software is written as a tab delimited file. Each line containing the chromosome and position of SNPs with diagnostic SNPs. Thus, it can be used to subset the original vcf file using vcftools' --positions <filename> option.
" 1>&2
exit 1
}

while getopts ":v:a:b:" i; do
    case "${i}" in
        v)
            v=${OPTARG}
            ;;
        a)
            a=${OPTARG}
            ;;
        b)
            b=${OPTARG}
            ;;
       *)
            usage
            ;;
    esac
done
shift $((OPTIND-1))

if [ -z "${v}" ] || [ -z "${a}" ] || [ -z "${b}" ]; then
    usage
fi

echo "Running DiagnoSNPs$version with the following input files"
echo "v = ${v}"
echo "a = ${a}"
echo "b = ${b}"
echo ""

echo "01.- Creating list of SNPs fixed for one allele for each group"
echo "Looking for fixed SNPs in ${a}"
vcftools --vcf ${v} --keep ${a} --max-maf 0 --hardy --out tmp_a
echo "Looking for fixed SNPs in ${b}"
vcftools --vcf ${v} --keep ${b} --max-maf 0 --hardy --out tmp_b
echo ""

echo "02.- Looking for SNPs that are fixed in both populations"
Comparing_fixed_SNPs.R
# Small format change
tr "," "\t" < shared.txt > shared_snps.txt
rm shared.txt tmp_a.hwe tmp_b.hwe
echo ""

echo "03.- HardyWeinberg testing fixed SNPs"
vcftools --vcf ${v} --keep ${a} --keep ${b} --positions shared_snps.txt --hardy --out HWe_snps
rm shared_snps.txt
echo ""

echo "04.- Deleting biallelic SNPs fixed for the same allele in both groups"
# Deleting_same_fixed_allele.R
# Another small format change
tr "," "\t" < HWe_snps.hwe > KD_vs_SM.DiagnoSNPs.txt
rm HWe_snps.hwe

#echo "Finished running DiagnoSNPs.sh. Your list of alternatively fixed SNPs is KD.DiagnoSNPs.txt"

--------------------------------------------------------------------------------------------------------------

# Comparing_fixed_SNPs.R
#!/usr/bin/env Rscript
# Comparing_fixed_SNPs.R script
# As part of the DiagnoSNPs package
# This script looks for shared fixed SNPs between two groups:

# Loading data
hwe <- list()
hwe$a <- read.table("tmp_a.hwe", header = T)
hwe$b <- read.table("tmp_b.hwe", header = T)

# Creating loci names
lapply(hwe, function(x) data.frame(x, names=paste(x$CHR, x$POS, sep=","))) -> hwe

# Looking for shared loci between species
shared <- intersect(hwe$a$names, hwe$b$names)
print("Some shared loci:")
head(shared)
print("Total number of shared fixed loci:")
length(shared)

# Writing file for shared loci
write(shared, "shared.txt", ncolumns = 1)

------------------------------------------------------------------------------------------------------

#Deleting_same_fixed_allele.R
#!/usr/bin/env Rscript
# Deleting_same_fixed_allele.R script
# As part of the DiagnoSNPs package
# This script deletes shared fixed allele SNPs between two groups:

# Loading data
fixed <- read.table("HWe_snps.hwe", header = T)

# Creating loci names
fixed$names <- paste(fixed$CHR, fixed$POS, sep = ",")
print("Total number of fixed loci:")
length(fixed$names)

# Taking out SNPs fixed for the same allele in both species (monomorphic for both groups)
fixed_filtered <- fixed[!fixed$ChiSq_HWE == 'NaN',]
message("Total number of bi-allelic SNPs fixed for opposite alleles between the two groups")
length(fixed_filtered$names)

# Writing file for shared loci
write(fixed_filtered$names, "diagnostic_snps.txt", ncolumns = 1)
