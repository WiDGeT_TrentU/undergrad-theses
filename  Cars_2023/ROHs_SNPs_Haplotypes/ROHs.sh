# print directly into terminal (makes .ped and .fam files)
module load plink2
plink2 --vcf N90.WTD.51.angsd.vcf --recode transpose --out N90.WTD.51 --allow-extra-chr

# print directly into terminal (find ROHs)
module load plink
plink --tfile N90.WTD.51 --homozyg-snp 50 --homozyg-kb 100 --homozyg-density 50 --homozyg-gap 1000 --homozyg-window-snp 50 --homozyg-window-het 3 --homozyg-window-missing 10 --homozyg-window-threshold 0.05 --allow-extra-chr --out N90.WTD.51 --homozyg-group

# Example of how to extract only the pools containing all target individuals
grep 'SP016\|SP035\|SP079\|SP093' N90.WTD.51.hom.overlap > Leucism.hom.overlap
awk '{print $1}' Leucism.hom.overlap | uniq -c | sort
grep -w 'S1\|S10\|S11\|S14\|S17\|S18\|S2\|S22\|S23\|S24\|S3\|S30\|S34\|S35\|S42\|S49\|S5\|S52\|S53\|S54\|S6\|S60\S9' N90.WTD.51.hom.overlap > pools_with_all_Leucism
