### Filter each scaffold by MAF
mkdir Filter_slurm
for f in $(ls *.vcf.gz | cut -f1 -d'.'| uniq)
do
echo ${f}
sbatch -o /home/bcars/projects/def-shaferab/bcars/thesis/sorted_bam/SHAPEIT/Filter_slurm/${f}-%A.out filter_MAF.sh ${f}
sleep 10
done

### filter_MAF.sh
#!/bin/bash
#SBATCH --account=rrg-shaferab
#SBATCH --job-name=FILTER
#SBATCH --mem 100M
#SBATCH --cpus-per-task 8
#SBATCH --time=00-01:59

module load vcftools

vcftools --gzvcf ${1}.vcf.gz --recode --maf 0.05 --out ${1}_maf5

### Run SHAPEIT2
module load shapeit/2.r904
mkdir phase_logs
for vcf in $(ls *maf5.recode.vcf)
do
sbatch -o phase_logs/$vcf-phase-%A.out --cpus-per-task 32 --mem=7G --time=0-95:59 shapeit_phase.sh ${vcf}
done

### shapeit_phase.sh
#!/bin/bash
#SBATCH --account=rrg-shaferab
#SBATCH --job-name=shapeit_phase
# phase data
scaff=$(echo ${1} | sed 's/-phased.*$//')
# Main run
shapeit -V ${1} -O shapeit_$scaff --output-log phase_logs/$scaff.main --force
# Convert back to phased vcf, containing only the phased sites that shapeit used
shapeit -convert --input-haps shapeit_$scaff --output-vcf phase_shapeit_$scaff --output-log phase_logs/$scaff.convert

### zip and index all filtered and phased scaffolds
#!/bin/bash
#SBATCH --account=rrg-shaferab
#SBATCH --job-name=tabix
#SBATCH --cpus-per-task=1
#SBATCH --mem=1G
#SBATCH --time=0-08:00 # time (DD-HH:MM)
 module load tabix
for f in `ls *.vcf`
do
bgzip $f
done

for f in `ls *.vcf.gz`
do
tabix -p vcf $f
done
#END

### hapLrt
#!/bin/bash
#SBATCH --account=rrg-shaferab
#SBATCH --job-name=hapLrt
#SBATCH --mem 1G
#SBATCH --cpus-per-task 8
#SBATCH --time=01-00:00
module load vcflib/1.0.3
#ALl of SPM vs North Mainland and Anticosti
hapLrt --target 30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45 --background 0,1,2,3,15,16,18,19,20,21,22,23,24,25,26,27,28 --type GT --file phase_shapeit_ref0000025_maf5.recode.vcf.gz >  SPM_phase_shapeit_ref0000025_maf5.hapLrt.txt 
#Repeat for each scaffold


### Filter outputted hapLrt File
# Step 1 - Unix - filter out stuff we don’t want
grep -e -1 -v phase_shapeit_ref0002869_maf5.hapLrt.txt | awk ‘{ if ($5 < 0.01) { print } }’ > SCAFfiltered.txt
# Step 2 - R - loop over positions to see how far apart the successive SNPs are
a<-read.table(“SCAFfiltered.txt”, header = FALSE)
for(i in 1:(nrow(a) - 1)) {
   a$V7[i] = a$V2[i+1] - a$V2[i]
}
write.table(a, file = “SCAFfilteredMOD.txt”,row.names = FALSE, col.names = FALSE, sep = “\t”)
# Step 3 - Unix - find breaks in SNPS 5000 kb apart and put in “break”
head -n 1 SCAFfilteredMOD.txt > start
tail -n 1 SCAFfilteredMOD.txt > end
awk ‘$7>5000{for(i=1;i<=NF;i++)$i=“BREAK”}1’ SCAFfilteredMOD.txt | grep BREAK -A 1 -B 1 > middle
cat start middle end | sed ‘/--/d’ > SCAFfilteredBED.txt
rm start end middle
step 4 - back to R to find length of blocks
b <-read.table(“SCAFfilteredBED.txt”, header = FALSE)
for(i in 1:(nrow(b) - 1)) {
   b$V8[i] = as.numeric(b$V2[i+1]) - as.numeric(b$V2[i])
}
head(b)
# Step 4 - excel - make table from data and find blocks >10000 kb long

