### Subset vcf by individuals
#!/bin/bash
#SBATCH --time=5:00:00
#SBATCH --account=rrg-shaferab
#SBATCH --cpus-per-task=16
#SBATCH --mem=300M
#SBATCH --job-name=vcf

module load vcftools

vcftools --gzvcf N90.WTD.51.angsd.phased.vcf.gz --keep SP.txt --non-ref-ac-any 1 --recode --recode-INFO-all --stdout > SP.subset.vcftools.vcf

------------------------------------------------------------------------------------------------
## Following tutorial https://pcingola.github.io/SnpEff/se_introduction/
## Edit configuration file as outlined in tutorial with the following
## wtd genome, version wtd_genome.fasta.masked
wtd : /project/6007974/mpmb38/snpEff_v5.0/snpEff/data/wtd/sequences.fa
wtd.genome : /project/6007974/mpmb38/snpEff_v5.0/snpEff/data/wtd/sequences.fa

--------------------------------------------------------------------------------------------------------

#!/bin/bash
#SBATCH --time=10:00:00
#SBATCH --account=rrg-shaferab
#SBATCH --cpus-per-task=32
#SBATCH --mem=0
#SBATCH --job-name=snpEff
#SBATCH --output=%x-%j.out

module load snpeff

java -Xmx120G -jar snpEff.jar -no INTERGENIC -no INTRAGENIC -ud 25000 wtd SP.subset.vcftools.vcf > SP_snpeff.vcf
mv snpEff_summary.html SP_snpEff_summary.html
mv snpEff_genes.txt SP_snpEff_genes.txt

---------------------------------------------------------------------------------------------------------------
### Extract desired fields, seperating mutliple fields in column by ","
#!/bin/bash
#SBATCH --time=10:00:00
#SBATCH --account=rrg-shaferab
#SBATCH --cpus-per-task=32
#SBATCH --mem=0
#SBATCH --job-name=snpEff
#SBATCH --output=%x-%j.out
java -jar SnpSift.jar extractFields SP_snpeff.vcf -s "," -e "." CHROM POS "ANN[*].EFFECT" "ANN[*].IMPACT" "ANN[*].DISTANCE" "ANN[*].GENE" "GEN[*].GT" > SP_snpsift.txt
