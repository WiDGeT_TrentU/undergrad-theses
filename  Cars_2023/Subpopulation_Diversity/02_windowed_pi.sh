#!/bin/bash
#SBATCH --account=rrg-shaferab
#SBATCH --job-name=sitespi
#SBATCH --cpus-per-task=8
#SBATCH --mem=24G
#SBATCH --time=0-3:59 # time (DD-HH:MM)

module load vcftools

vcftools --vcf WTD.51.angsd.vcf --keep North_Mainland.txt --window-pi 10000 --out North.pi
vcftools --vcf WTD.51.angsd.vcf --keep South_Mainland.txt --window-pi 10000 --out South.pi
vcftools --vcf WTD.51.angsd.vcf --keep KD.txt --window-pi 10000 --out KD.pi
vcftools --vcf WTD.51.angsd.vcf --keep SP.txt --window-pi 10000 --out SP.pi
vcftools --vcf WTD.51.angsd.vcf --keep AC.txt --window-pi 10000 --out AC.pi
