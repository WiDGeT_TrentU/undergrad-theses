#!/bin/bash
#SBATCH --account=rrg-shaferab
#SBATCH --job-name=FST
#SBATCH --cpus-per-task=8
#SBATCH --mem=24G
#SBATCH --time=0-7:59 # time (DD-HH:MM)

module load vcftools

vcftools --vcf WTD.51.angsd.vcf --weir-fst-pop AC.txt --weir-fst-pop SP.txt --fst-window-size 10000 --out AC_vs_SP_FST.stats
vcftools --vcf WTD.51.angsd.vcf --weir-fst-pop AC.txt --weir-fst-pop KD.txt --fst-window-size 10000 --out AC_vs_KD_FST.stats
vcftools --vcf WTD.51.angsd.vcf --weir-fst-pop AC.txt --weir-fst-pop North_Mainland.txt --fst-window-size 10000 --out AC_vs_North_FST.stats
vcftools --vcf WTD.51.angsd.vcf --weir-fst-pop AC.txt --weir-fst-pop South_Mainland.txt --fst-window-size 10000 --out AC_vs_South_FST.stats
vcftools --vcf WTD.51.angsd.vcf --weir-fst-pop SP.txt --weir-fst-pop KD.txt --fst-window-size 10000 --out SP_vs_KD_FST.stats
vcftools --vcf WTD.51.angsd.vcf --weir-fst-pop SP.txt --weir-fst-pop North_Mainland.txt --fst-window-size 10000 --out SP_vs_North_FST.stats
vcftools --vcf WTD.51.angsd.vcf --weir-fst-pop SP.txt --weir-fst-pop South_Mainland.txt --fst-window-size 10000 --out SP_vs_South_FST.stats
vcftools --vcf WTD.51.angsd.vcf --weir-fst-pop South_Mainland.txt --weir-fst-pop North_Mainland.txt --fst-window-size 10000 --out South_vs_North_FST.stats
vcftools --vcf WTD.51.angsd.vcf --weir-fst-pop KD.txt --weir-fst-pop South_Mainland.txt --fst-window-size 10000 --out KD_vs_South_FST.stats
vcftools --vcf WTD.51.angsd.vcf --weir-fst-pop KD.txt --weir-fst-pop North_Mainland.txt --fst-window-size 10000 --out North_vs_KD_FST.stats
