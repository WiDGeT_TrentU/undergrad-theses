#!/bin/bash
#SBATCH --account=rrg-shaferab
#SBATCH --job-name=TajimaD
#SBATCH --cpus-per-task=8
#SBATCH --mem=10G
#SBATCH --time=0-04:59 # time (DD-HH:MM)

module load vcftools

vcftools --vcf WTD.51.angsd.vcf --keep North_Mainland.txt --TajimaD 10000 --out North.D
vcftools --vcf WTD.51.angsd.vcf --keep South_Mainland.txt --TajimaD 10000 --out South.D
vcftools --vcf WTD.51.angsd.vcf --keep KD.txt --TajimaD 10000 --out KD.D
vcftools --vcf WTD.51.angsd.vcf --keep SP.txt --TajimaD 10000 --out SP.D
vcftools --vcf WTD.51.angsd.vcf --keep AC.txt --TajimaD 10000 --out AC.D
