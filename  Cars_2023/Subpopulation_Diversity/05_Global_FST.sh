#!/bin/bash
#SBATCH --account=rrg-shaferab
#SBATCH --job-name=FST
#SBATCH --cpus-per-task=8
#SBATCH --mem=24G
#SBATCH --time=0-2:00 # time (DD-HH:MM)

module load vcftools

vcftools --vcf WTD.51.angsd.vcf --weir-fst-pop AC.txt --weir-fst-pop KD.txt --weir-fst-pop North_Mainland.txt --weir-fst-pop South_Mainland.txt --weir-fst-pop SP.txt --fst-window-size 10000 --out FST_global.stats
