#!/bin/bash
#SBATCH --account=rrg-shaferab
#SBATCH --job-name=bwa-bam-sort
#SBATCH --cpus-per-task=32
#SBATCH --mem=64000
#SBATCH --time=0-07:59 # time (DD-HH:MM)
module load bwa
module load samtools
echo ${1}

#use for normal files
bwa mem -M -t 16 -R "@RG\\tID:${1}\\tSM:${1}\\tLB:${1}\\tPL:ILLUMINA" \
${2} ${3}/${1}_trim_R1_001.fastq.gz ${3}/${1}_trim_R2_001.fastq.gz | \
samtools sort -@ 16 -o ${4}/${1}.sorted_reads.bam &&
samtools flagstat ${4}/${1}.sorted_reads.bam > ${4}/${1}.sorted_reads.flagstat
#END
