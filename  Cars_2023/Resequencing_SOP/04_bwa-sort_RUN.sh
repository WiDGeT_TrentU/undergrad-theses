#!/bin/bash
#MAP_script_RUN

mkdir sorted_bam

# Genome location
g="/home/bcars/projects/def-shaferab/bcars/thesis/odocoileus_virginianus_NEW.fa"

# Directory for "raw" or trimmed fastq files, note the lack of last slash
dir_raw="/home/bcars/projects/def-shaferab/bcars/thesis"

# Output directory for bams, note the lack of last slash
dir_bam="/home/bcars/projects/def-shaferab/bcars/thesis/sorted_bam"

for f in $(ls *trim*.fastq.gz | cut -f1-4 -d'_'| uniq) #for Odo_ON_X samples cut -f1-5 -d'_'
do
echo ${f}
sbatch -o /home/bcars/projects/def-shaferab/bcars/thesis/${f}-%A.out 05_bwa-sort.sh ${f} ${g} ${dir_raw} ${dir_bam}
sleep 10
done
