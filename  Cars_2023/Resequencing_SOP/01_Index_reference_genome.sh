#rename header to match annotation file
 sed -e 's/\parts_to_remove//' odocoileus_virginianus_OLD.fa > odocoileus_virginianus_NEW.fa

#!/bin/bash
#SBATCH --account=rrg-shaferab
#SBATCH --job-name=index
#SBATCH --mem=8G
#SBATCH --time==0-03:00 # time (DD-HH:MM)

module load bwa
module load picard
module load gatk/3.8
module load samtools

java -Xmx8G -jar $EBROOTPICARD/picard.jar CreateSequenceDictionary R=odocoileus_virginianus_NEW.fa O=odocoileus_virginianus.dict
samtools faidx odocoileus_virginianus_NEW.fa
bwa index odocoileus_virginianus_NEW.fa -a bwtsw
