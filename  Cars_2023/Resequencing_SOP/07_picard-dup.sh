#!/bin/bash
#SBATCH --account=rrg-shaferab
#SBATCH --job-name=remove-dups
#SBATCH --cpus-per-task=30
#SBATCH --mem=125G
#SBATCH --time=0-02:00 # time (DD-HH:MM)

module load picard
module load samtools
module load sambamba
echo ${1}
java -Xmx120G -jar $EBROOTPICARD/picard.jar MarkDuplicates \
INPUT=/home/bcars/projects/def-shaferab/bcars/thesis/sorted_bam/${1}.sorted_reads.bam \
OUTPUT=/home/bcars/projects/def-shaferab/bcars/thesis/sorted_bam/${1}.deduped_reads.bam \
USE_JDK_DEFLATER=true USE_JDK_INFLATER=true \
ASSUME_SORT_ORDER=coordinate \
REMOVE_DUPLICATES=true REMOVE_SEQUENCING_DUPLICATES=true \
METRICS_FILE=/home/bcars/projects/def-shaferab/bcars/thesis/sorted_bam/${1}.deduped.picard &&
/home/bcars/projects/def-shaferab/bcars/thesis/sorted_bam/sambamba-0.8.2-linux-amd64-static flagstat \
--nthreads=30 \
/home/bcars/projects/def-shaferab/bcars/thesis/sorted_bam/${1}.deduped_reads.bam \
> /home/bcars/projects/def-shaferab/bcars/thesis/sorted_bam/${1}.deduped_reads.flagstat
#END
