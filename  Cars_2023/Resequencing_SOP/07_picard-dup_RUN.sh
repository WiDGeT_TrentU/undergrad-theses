#Picard MarkDuplicates

mkdir dups_slurm
for f in $(ls *.bam | sed 's/.sorted_reads.bam//' | sort -u)
do
echo ${f}
sbatch -o /home/bcars/projects/def-shaferab/bcars/thesis/sorted_bam/dups_slurm/${f}-%A.out 07_picard-dup.sh ${f}
sleep 10
done
