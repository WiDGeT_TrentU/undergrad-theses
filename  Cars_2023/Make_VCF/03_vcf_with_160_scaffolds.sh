# Exctract 160 scaffolds that make up the N90 from vcf
 module load bioawk
 data=/home/bcars/projects/def-shaferab/bcars/thesis/sorted_bam
ref=/home/bcars/projects/def-shaferab/bcars/thesis
vcf=$data/WTD.51.angsd.vcf
bioawk -c fastx -t '{print $name, length($seq)}' $ref/odocoileus_virginianus.fasta | sort -k2 -n | tail -n 160 | cut -f 1 > Scaffolds.txt

#Make file 03_vcf_split.sh to split vcf
#!/bin/bash
#SBATCH --account=rrg-shaferab
#SBATCH --job-name=vcf_split

data=/home/bcars/projects/def-shaferab/bcars/thesis/sorted_bam
ref=/home/bcars/projects/def-shaferab/bcars/thesis
vcf=$data/WTD.51.angsd.vcf

grep "^#" ${1} > vcf_header

while read line
do
grep -v "^#" ${1} | awk -v scaff=$line '{ if ( $1==scaff ) print }' | cat vcf_header - > vcf_chr/$line.vcf
done < Scaffolds.txt

#copy and paste into terminal:
 # split vcf
 data=/home/bcars/projects/def-shaferab/bcars/thesis/sorted_bam
 ref=/home/bcars/projects/def-shaferab/bcars/thesis
 mkdir vcf_chr
 vcf=WTD.51.angsd.vcf
 sbatch -o vcf_split-%A.out --cpus-per-task 8 --mem=2G --time=16:00:00 03_vcf_split.sh ${vcf}

#Zip files and make indices
#!/bin/bash
#SBATCH --account=rrg-shaferab
#SBATCH --job-name=tabix
#SBATCH --cpus-per-task=1
#SBATCH --mem=1G
#SBATCH --time=0-03:00 # time (DD-HH:MM)
 module load tabix
for f in `ls *.vcf`
do
bgzip $f
done

for f in `ls *.vcf.gz`
do
tabix -p vcf $f
done
#END

# Assemble vcf of 160 Scaffolds
module load vcftools
vcf-concat ref0000025.vcf.gz ref0000068.vcf.gz ref0000086.vcf.gz ref0000102.vcf.gz ref0000146.vcf.gz ref0000154.vcf.gz ref0000159.vcf.gz ref0000164.vcf.gz ref0000184.vcf.gz ref0000239.vcf.gz ref0000259.vcf.gz ref0000262.vcf.gz ref0000279.vcf.gz ref0000297.vcf.gz ref0000312.vcf.gz ref0000330.vcf.gz ref0000334.vcf.gz ref0000335.vcf.gz ref0000357.vcf.gz ref0000366.vcf.gz ref0000371.vcf.gz ref0000384.vcf.gz ref0000393.vcf.gz ref0000413.vcf.gz ref0000438.vcf.gz ref0000440.vcf.gz ref0000450.vcf.gz ref0000453.vcf.gz ref0000457.vcf.gz ref0000461.vcf.gz ref0000466.vcf.gz ref0000505.vcf.gz ref0000511.vcf.gz ref0000516.vcf.gz ref0000538.vcf.gz ref0000546.vcf.gz ref0000547.vcf.gz ref0000555.vcf.gz ref0000647.vcf.gz ref0000677.vcf.gz ref0000684.vcf.gz ref0000773.vcf.gz ref0000791.vcf.gz ref0000799.vcf.gz ref0000837.vcf.gz ref0000845.vcf.gz ref0000849.vcf.gz ref0000856.vcf.gz ref0000858.vcf.gz ref0000881.vcf.gz ref0000889.vcf.gz ref0000892.vcf.gz ref0000902.vcf.gz ref0000963.vcf.gz ref0000970.vcf.gz ref0000998.vcf.gz ref0001062.vcf.gz ref0001069.vcf.gz ref0001090.vcf.gz ref0001114.vcf.gz ref0001122.vcf.gz ref0001139.vcf.gz ref0001148.vcf.gz ref0001151.vcf.gz ref0001155.vcf.gz ref0001167.vcf.gz ref0001199.vcf.gz ref0001234.vcf.gz ref0001271.vcf.gz ref0001279.vcf.gz ref0001281.vcf.gz ref0001370.vcf.gz ref0001398.vcf.gz ref0001412.vcf.gz ref0001414.vcf.gz ref0001468.vcf.gz ref0001545.vcf.gz ref0001618.vcf.gz ref0001631.vcf.gz ref0001634.vcf.gz ref0001640.vcf.gz ref0001660.vcf.gz ref0001666.vcf.gz ref0001695.vcf.gz ref0001746.vcf.gz ref0001766.vcf.gz ref0001786.vcf.gz ref0001811.vcf.gz ref0001816.vcf.gz ref0001825.vcf.gz ref0001836.vcf.gz ref0001868.vcf.gz ref0001876.vcf.gz ref0001886.vcf.gz ref0001898.vcf.gz ref0001904.vcf.gz ref0001924.vcf.gz ref0001931.vcf.gz ref0001963.vcf.gz ref0001994.vcf.gz ref0002008.vcf.gz ref0002067.vcf.gz ref0002075.vcf.gz ref0002083.vcf.gz ref0002123.vcf.gz ref0002124.vcf.gz ref0002149.vcf.gz ref0002179.vcf.gz ref0002192.vcf.gz ref0002196.vcf.gz ref0002230.vcf.gz ref0002232.vcf.gz ref0002244.vcf.gz ref0002250.vcf.gz ref0002253.vcf.gz ref0002266.vcf.gz ref0002332.vcf.gz ref0002374.vcf.gz ref0002388.vcf.gz ref0002391.vcf.gz ref0002419.vcf.gz ref0002425.vcf.gz ref0002430.vcf.gz ref0002453.vcf.gz ref0002456.vcf.gz ref0002460.vcf.gz ref0002478.vcf.gz ref0002485.vcf.gz ref0002500.vcf.gz ref0002507.vcf.gz ref0002510.vcf.gz ref0002511.vcf.gz ref0002516.vcf.gz ref0002547.vcf.gz ref0002550.vcf.gz ref0002556.vcf.gz ref0002605.vcf.gz ref0002608.vcf.gz ref0002624.vcf.gz ref0002630.vcf.gz ref0002635.vcf.gz ref0002644.vcf.gz ref0002645.vcf.gz ref0002674.vcf.gz ref0002699.vcf.gz ref0002706.vcf.gz ref0002712.vcf.gz ref0002718.vcf.gz ref0002725.vcf.gz ref0002752.vcf.gz ref0002754.vcf.gz ref0002783.vcf.gz ref0002788.vcf.gz ref0002813.vcf.gz ref0002823.vcf.gz ref0002837.vcf.gz ref0002846.vcf.gz ref0002863.vcf.gz ref0002869.vcf.gz ref0002875.vcf.gz > N90.WTD.51.angsd.vcf.gz
