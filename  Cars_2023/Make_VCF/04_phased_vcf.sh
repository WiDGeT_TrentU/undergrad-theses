#!/bin/bash
#SBATCH --account=rrg-shaferab
#SBATCH --job-name=beagle-phase
#SBATCH --mem 32G
#SBATCH --cpus-per-task 16
#SBATCH --time=04-00:00

#angsd vcf
#wget https://faculty.washington.edu/browning/beagle/beagle.27Jan18.7e1.jar
java -Xmx32g -Djava.awt.headless=true -jar beagle.27Jan18.7e1.jar gtgl="N90.WTD.51.angsd.vcf" impute=false gprobs=TRUE out=N90.WTD.51.angsd.phased
