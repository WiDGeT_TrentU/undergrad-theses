#!/bin/bash
#SBATCH --account=rrg-shaferab
#SBATCH --job-name=bcftovcf
#SBATCH --cpus-per-task=10
#SBATCH --mem=24G
#SBATCH --time=0-2:59 # time (DD-HH:MM)

module load bcftools

bcftools convert -O v -o WTD.51.angsd.vcf WTD.51.angsd.bcf
