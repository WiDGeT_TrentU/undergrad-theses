### Figure S2 (R)
``{r}
library(tidyverse)
library(ggplot2)
library(gghalves)
library(forcats)
library(patchwork)
library(scales)

data <- read.table(file = "./SNPeff Data.txt", sep = "", stringsAsFactors=FALSE, header = T)
data

Location <- rep(NA, length(data$Group))
Location[grep("LSPM", data$Group)] <- "Saint Pierre & Miquelon"
Location[grep("MSPM", data$Group)] <- "Saint Pierre & Miquelon"
Location[grep("NSPM", data$Group)] <- "Saint Pierre & Miquelon"
Location[grep("KD", data$Group)] <- "Florida Keys"
Location[grep("AC", data$Group)] <- "Anticosti"
Location[grep("NM", data$Group)] <- "North Mainland"
Location[grep("SM", data$Group)] <- "South Mainland"

Phenotype <- rep(NA, length(data$Group))
Phenotype[grep("LSPM", data$Group)] <- "Leucism"
Phenotype[grep("MSPM", data$Group)] <- "Malocclusions"
Phenotype[grep("NSPM", data$Group)] <- "Normal"
Phenotype[grep("KD", data$Group)] <- "Small Stature"
Phenotype[grep("AC", data$Group)] <- "Normal"
Phenotype[grep("NM", data$Group)] <- "Normal"
Phenotype[grep("SM", data$Group)] <- "Normal"

Phenotype_location <- paste0(Phenotype, "_", Location)
data <- as.tibble(data.frame(data, Phenotype, Location, Phenotype_location)) 
data$Location <- factor(data$Location, 
                     levels = c('Florida Keys', 'Saint Pierre & Miquelon', 'Anticosti', 'North Mainland', 'South Mainland' ))

plot1 <- ggplot(data, aes(x = Location, y=HIGH,
                              percent_genome, fill = Phenotype)) +
  geom_half_point(side = "l", shape = 21, alpha = 0.5, stroke = 0.1, size =4,
                  transformation_params = list(height = 0, width = 1.3, seed = 1)) +
  geom_half_boxplot(side = "r", outlier.color = NA,
                    width = 0.6, lwd = 0.3, color = "black",
                    alpha = 0.8) +
  scale_fill_manual(values = c("#FFC107","#0051B9","black","#D81B1B"), name = "Phenotype") + 
   labs(y = "High", x = " ") + scale_x_discrete(labels = label_wrap(13)) + theme(axis.text.x = element_blank(), axis.ticks.x = element_blank())
plot2 <- ggplot(data, aes(x = Location, y=MODERATE,
                              percent_genome, fill = Phenotype)) +
  geom_half_point(side = "l", shape = 21, alpha = 0.5, stroke = 0.1, size =4,
                  transformation_params = list(height = 0, width = 1.3, seed = 1)) +
  geom_half_boxplot(side = "r", outlier.color = NA,
                    width = 0.6, lwd = 0.3, color = "black",
                    alpha = 0.8) +
  scale_fill_manual(values = c("#FFC107","#0051B9","black","#D81B1B"), name = "Phenotype") + 
   labs(y = "Moderate", x = " ")  + scale_x_discrete(labels = label_wrap(13)) + theme(axis.text.x = element_blank(), axis.ticks.x = element_blank())
plot3 <- ggplot(data, aes(x = Location, y=LOW,
                              percent_genome, fill = Phenotype)) +
  geom_half_point(side = "l", shape = 21, alpha = 0.5, stroke = 0.1, size =4,
                  transformation_params = list(height = 0, width = 1.3, seed = 1)) +
  geom_half_boxplot(side = "r", outlier.color = NA,
                    width = 0.6, lwd = 0.3, color = "black",
                    alpha = 0.8) +
  scale_fill_manual(values = c("#FFC107","#0051B9","black","#D81B1B"), name = "Phenotype") + 
   labs(y = "Low", x = " ")  + scale_x_discrete(labels = label_wrap(13))
pdf(file = "./HML2.pdf")
wrap_plots(plot1, plot2, plot3, nrow = 3) + plot_layout(guides = "collect")
dev.off()

```
