### Figure 3 A

```{r}
library(tidyverse)
library(ggplot2)
library(gghalves)
library(forcats)
library(scales)
data <- read.table(file = "./N90.WTD.51.hom.indiv", quote="", stringsAsFactors=FALSE, header = T)

#add species
Phenotypes <- rep(NA, length(data$IID))
Phenotypes[grep("AC2_S85_L004.unique_reads.bam", data$IID)] <- "Normal"
Phenotypes[grep("AC3_S86_L004.unique_reads.bam", data$IID)] <- "Normal"
Phenotypes[grep("AC4_S87_L004.unique_reads.bam", data$IID)] <- "Normal"
Phenotypes[grep("AC7_S88_L004.unique_reads.bam", data$IID)] <- "Normal"
Phenotypes[grep("Odo_AL3_S1_L001.unique_reads.bam", data$IID)] <- "Normal"
Phenotypes[grep("Odo_ME1_S11_L002.unique_reads.bam", data$IID)] <- "Normal"
Phenotypes[grep("Odo_NB1_S13_L008.unique_reads.bam", data$IID)] <- "Normal"
Phenotypes[grep("Odo_NC1_S5_L001.unique_reads.bam", data$IID)] <- "Normal"
Phenotypes[grep("Odo_NS1_S6_L001.unique_reads.bam", data$IID)] <- "Normal"
Phenotypes[grep("Odo_NY1_S3_L001.unique_reads.bam", data$IID)] <- "Normal"
Phenotypes[grep("Odo_NY2_S15_L002.unique_reads.bam", data$IID)] <- "Normal"
Phenotypes[grep("Odo_ON2R_S9_L008.unique_reads.bam", data$IID)] <- "Normal"
Phenotypes[grep("Odo_ON6_S12_L008.unique_reads.bam", data$IID)] <- "Normal"
Phenotypes[grep("Odo_ON_X1_S11_L004.unique_reads.bam", data$IID)] <- "Normal"
Phenotypes[grep("Odo_ON_X2_S12_L004.unique_reads.bam", data$IID)] <- "Normal"
Phenotypes[grep("Odo_ON_X3_S13_L004.unique_reads.bam", data$IID)] <- "Normal"
Phenotypes[grep("Odo_ON_X4_S14_L004.unique_reads.bam", data$IID)] <- "Normal"
Phenotypes[grep("Odo_QC1_S12_L008.unique_reads.bam", data$IID)] <- "Normal"
Phenotypes[grep("Odo_QC2_S_L_merged.unique_reads.bam", data$IID)] <- "Normal"
Phenotypes[grep("Odo_SC1_S6_L001.unique_reads.bam", data$IID)] <- "Normal"
Phenotypes[grep("Odo_TX18_S9_L003.unique_reads.bam", data$IID)] <- "Normal"
Phenotypes[grep("Odo_TX20_S10_L003.unique_reads.bam", data$IID)] <- "Normal"
Phenotypes[grep("Odo_TX23_S17_L004.unique_reads.bam", data$IID)] <- "Normal"
Phenotypes[grep("Odo_TX3_S8_L003.unique_reads.bam", data$IID)] <- "Normal"
Phenotypes[grep("Odo_VA3_S14_L008.unique_reads.bam", data$IID)] <- "Normal"
Phenotypes[grep("Odo_Key7_S79_L004.unique_reads.bam", data$IID)] <- "Small Stature"
Phenotypes[grep("Odo_Key8_S80_L004.unique_reads.bam", data$IID)] <- "Small Stature"
Phenotypes[grep("Odo_Key9_S81_L004.unique_reads.bam", data$IID)] <- "Small Stature"
Phenotypes[grep("Odo_Key10_S82_L004.unique_reads.bam", data$IID)] <- "Small Stature"
Phenotypes[grep("Odo_Key15_S83_L004.unique_reads.bam", data$IID)] <- "Small Stature"
Phenotypes[grep("Odo_Key17_S84_L004.unique_reads.bam", data$IID)] <- "Small Stature"
Phenotypes[grep("Odo_Key1_S2_L001.unique_reads.bam", data$IID)] <- "Small Stature"
Phenotypes[grep("Odo_Key3_S_L_merged.unique_reads.bam", data$IID)] <- "Small Stature"
Phenotypes[grep("Odo_Key4_S4_L001.unique_reads.bam", data$IID)] <- "Small Stature"
Phenotypes[grep("Odo_Key5_S5_L001.unique_reads.bam", data$IID)] <- "Small Stature"
Phenotypes[grep("SP016_S64_L004.unique_reads.bam", data$IID)] <- "Leucism" 
Phenotypes[grep("SP030_S75_L004.unique_reads.bam", data$IID)] <- "Normal"
Phenotypes[grep("SP032_S68_L004.unique_reads.bam", data$IID)] <- "Malocclusions"
Phenotypes[grep("SP035_S65_L004.unique_reads.bam", data$IID)] <- "Leucism"
Phenotypes[grep("SP038_S69_L004.unique_reads.bam", data$IID)] <- "Malocclusions"
Phenotypes[grep("SP042_S76_L004.unique_reads.bam", data$IID)] <- "Normal"
Phenotypes[grep("SP050_S77_L004.unique_reads.bam", data$IID)] <- "Normal"
Phenotypes[grep("SP051_S78_L004.unique_reads.bam", data$IID)] <- "Normal"
Phenotypes[grep("SP056_S70_L004.unique_reads.bam", data$IID)] <- "Malocclusions"
Phenotypes[grep("SP069_S71_L004.unique_reads.bam", data$IID)] <- "Malocclusions"
Phenotypes[grep("SP079_S66_L004.unique_reads.bam", data$IID)] <- "Leucism"
Phenotypes[grep("SP084_S72_L004.unique_reads.bam", data$IID)] <- "Malocclusions"
Phenotypes[grep("SP090_S73_L004.unique_reads.bam", data$IID)] <- "Malocclusions"
Phenotypes[grep("SP093_S67_L004.unique_reads.bam", data$IID)] <- "Leucism"
Phenotypes[grep("SP095_S74_L004.unique_reads.bam", data$IID)] <- "Malocclusions"
Phenotypes[grep("Odo_SP008_S2_L001.unique_reads.bam", data$IID)] <- "Normal"

#add location
Location <- rep(NA, length(data$IID))
Location[grep("AC", data$IID)] <- "Anticosti"
Location[grep("Odo_ME1_S11_L002.unique_reads.bam", data$IID)] <- "North Mainland"
Location[grep("Odo_NB1_S13_L008.unique_reads.bam", data$IID)] <- "North Mainland"
Location[grep("Odo_NS1_S6_L001.unique_reads.bam", data$IID)] <- "North Mainland"
Location[grep("Odo_NY1_S3_L001.unique_reads.bam", data$IID)] <- "North Mainland"
Location[grep("Odo_NY2_S15_L002.unique_reads.bam", data$IID)] <- "North Mainland"
Location[grep("Odo_ON2R_S9_L008.unique_reads.bam", data$IID)] <- "North Mainland"
Location[grep("Odo_ON6_S12_L008.unique_reads.bam", data$IID)] <- "North Mainland"
Location[grep("Odo_ON_X1_S11_L004.unique_reads.bam", data$IID)] <- "North Mainland"
Location[grep("Odo_ON_X2_S12_L004.unique_reads.bam", data$IID)] <- "North Mainland"
Location[grep("Odo_ON_X3_S13_L004.unique_reads.bam", data$IID)] <- "North Mainland"
Location[grep("Odo_ON_X4_S14_L004.unique_reads.bam", data$IID)] <- "North Mainland"
Location[grep("Odo_QC1_S12_L008.unique_reads.bam", data$IID)] <- "North Mainland"
Location[grep("Odo_QC2_S_L_merged.unique_reads.bam", data$IID)] <- "North Mainland"
Location[grep("Odo_AL3_S1_L001.unique_reads.bam", data$IID)] <- "South Mainland"
Location[grep("Odo_NC1_S5_L001.unique_reads.bam", data$IID)] <- "South Mainland"
Location[grep("Odo_SC1_S6_L001.unique_reads.bam", data$IID)] <- "South Mainland"
Location[grep("Odo_TX18_S9_L003.unique_reads.bam", data$IID)] <- "South Mainland"
Location[grep("Odo_TX20_S10_L003.unique_reads.bam", data$IID)] <- "South Mainland"
Location[grep("Odo_TX23_S17_L004.unique_reads.bam", data$IID)] <- "South Mainland"
Location[grep("Odo_TX3_S8_L003.unique_reads.bam", data$IID)] <- "South Mainland"
Location[grep("Odo_VA3_S14_L008.unique_reads.bam", data$IID)] <- "South Mainland"
Location[grep("K", data$IID)] <- "Florida Keys"
Location[grep("SP", data$IID)] <- "Saint Pierre & Miquelon"

#add Individuals
Indiv <- rep(NA, length(data$IID))
Indiv[grep("AC2_S85_L004.unique_reads.bam", data$IID)] <- "Odo_AC2"
Indiv[grep("AC3_S86_L004.unique_reads.bam", data$IID)] <- "Odo_AC3"
Indiv[grep("AC4_S87_L004.unique_reads.bam", data$IID)] <- "Odo_AC4"
Indiv[grep("AC7_S88_L004.unique_reads.bam", data$IID)] <- "Odo_AC7"
Indiv[grep("Odo_AL3_S1_L001.unique_reads.bam", data$IID)] <- "Odo_AL3"
Indiv[grep("Odo_ME1_S11_L002.unique_reads.bam", data$IID)] <- "Odo_ME1"
Indiv[grep("Odo_NB1_S13_L008.unique_reads.bam", data$IID)] <- "Odo_NB1"
Indiv[grep("Odo_NC1_S5_L001.unique_reads.bam", data$IID)] <- "Odo_NC1"
Indiv[grep("Odo_NS1_S6_L001.unique_reads.bam", data$IID)] <- "Odo_NS1"
Indiv[grep("Odo_NY1_S3_L001.unique_reads.bam", data$IID)] <- "Odo_NY1"
Indiv[grep("Odo_NY2_S15_L002.unique_reads.bam", data$IID)] <- "Odo_NY2"
Indiv[grep("Odo_ON2R_S9_L008.unique_reads.bam", data$IID)] <- "Odo_ON2R"
Indiv[grep("Odo_ON6_S12_L008.unique_reads.bam", data$IID)] <- "Odo_ON6"
Indiv[grep("Odo_ON_X1_S11_L004.unique_reads.bam", data$IID)] <- "Odo_ON_X1"
Indiv[grep("Odo_ON_X2_S12_L004.unique_reads.bam", data$IID)] <- "Odo_ON_X2"
Indiv[grep("Odo_ON_X3_S13_L004.unique_reads.bam", data$IID)] <- "Odo_ON_X3"
Indiv[grep("Odo_ON_X4_S14_L004.unique_reads.bam", data$IID)] <- "Odo_ON_X4"
Indiv[grep("Odo_QC1_S12_L008.unique_reads.bam", data$IID)] <- "Odo_QC1"
Indiv[grep("Odo_QC2_S_L_merged.unique_reads.bam", data$IID)] <- "Odo_QC2"
Indiv[grep("Odo_SC1_S6_L001.unique_reads.bam", data$IID)] <- "Odo_SC1"
Indiv[grep("Odo_TX18_S9_L003.unique_reads.bam", data$IID)] <- "Odo_TX18"
Indiv[grep("Odo_TX20_S10_L003.unique_reads.bam", data$IID)] <- "Odo_TX20"
Indiv[grep("Odo_TX23_S17_L004.unique_reads.bam", data$IID)] <- "Odo_TX23"
Indiv[grep("Odo_TX3_S8_L003.unique_reads.bam", data$IID)] <- "Odo_TX3"
Indiv[grep("Odo_VA3_S14_L008.unique_reads.bam", data$IID)] <- "Odo_VA3"
Indiv[grep("Odo_Key7_S79_L004.unique_reads.bam", data$IID)] <- "Odo_Key7"
Indiv[grep("Odo_Key8_S80_L004.unique_reads.bam", data$IID)] <- "Odo_Key8"
Indiv[grep("Odo_Key9_S81_L004.unique_reads.bam", data$IID)] <- "Odo_Key9"
Indiv[grep("Odo_Key10_S82_L004.unique_reads.bam", data$IID)] <- "Odo_Key10"
Indiv[grep("Odo_Key15_S83_L004.unique_reads.bam", data$IID)] <- "Odo_Key15"
Indiv[grep("Odo_Key17_S84_L004.unique_reads.bam", data$IID)] <- "Odo_Key17"
Indiv[grep("Odo_Key1_S2_L001.unique_reads.bam", data$IID)] <- "Odo_Key1"
Indiv[grep("Odo_Key3_S_L_merged.unique_reads.bam", data$IID)] <- "Odo_Key3"
Indiv[grep("Odo_Key4_S4_L001.unique_reads.bam", data$IID)] <- "Odo_Key4"
Indiv[grep("Odo_Key5_S5_L001.unique_reads.bam", data$IID)] <- "Odo_Key5"
Indiv[grep("SP016_S64_L004.unique_reads.bam", data$IID)] <- "Odo_SP016" 
Indiv[grep("SP030_S75_L004.unique_reads.bam", data$IID)] <- "Odo_SP030"
Indiv[grep("SP032_S68_L004.unique_reads.bam", data$IID)] <- "Odo_SP032"
Indiv[grep("SP035_S65_L004.unique_reads.bam", data$IID)] <- "Odo_SP035"
Indiv[grep("SP038_S69_L004.unique_reads.bam", data$IID)] <- "Odo_SP038"
Indiv[grep("SP042_S76_L004.unique_reads.bam", data$IID)] <- "Odo_SP042"
Indiv[grep("SP050_S77_L004.unique_reads.bam", data$IID)] <- "Odo_SP050"
Indiv[grep("SP051_S78_L004.unique_reads.bam", data$IID)] <- "Odo_SP051"
Indiv[grep("SP056_S70_L004.unique_reads.bam", data$IID)] <- "Odo_SP056"
Indiv[grep("SP069_S71_L004.unique_reads.bam", data$IID)] <- "Odo_SP069"
Indiv[grep("SP079_S66_L004.unique_reads.bam", data$IID)] <- "Odo_SP079"
Indiv[grep("SP084_S72_L004.unique_reads.bam", data$IID)] <- "Odo_SP084"
Indiv[grep("SP090_S73_L004.unique_reads.bam", data$IID)] <- "Odo_SP090"
Indiv[grep("SP093_S67_L004.unique_reads.bam", data$IID)] <- "Odo_SP093"
Indiv[grep("SP095_S74_L004.unique_reads.bam", data$IID)] <- "Odo_SP095"
Indiv[grep("Odo_SP008_S2_L001.unique_reads.bam", data$IID)] <- "Odo_SP008"


# combine - if you want to plot each in different colours
Phenotypes_location <- paste0(Phenotypes, "_", Location)
data <- as_tibble(data.frame(data, Phenotypes, Location, Phenotypes_location)) 
plot <- ggplot(data, aes(x = Location, y = KB/2546278.587*100, col = Phenotypes, shape = Location)) +
   geom_boxplot(fill = "transparent") + geom_jitter() + scale_shape_manual(values=c(8, 17, 19, 18, 15))+ ylab("FROH") + xlab("Individual") + scale_colour_manual(values = c("#FFC107","#0051B9","#D81B1B","black"), breaks=c("Leucism", "Malocclusions", "Small Stature", "Normal"), labels=c("Leucism", "Malocclusions", "Small Stature", "Normal"))
plot

data$Location <- factor(data$Location, 
                     levels = c('Florida Keys', 'Saint Pierre & Miquelon', 'Anticosti', 'North Mainland', 'South Mainland' ))
froh_plot <- ggplot(data, aes(x = Location, y = KB/2546278.587*100,
                              percent_genome, fill = Phenotypes)) +
  geom_half_point(side = "l", shape = 21, alpha = 0.5, stroke = 0.1, size =4,
                  transformation_params = list(height = 0, width = 1.3, seed = 1)) +
  geom_half_boxplot(side = "r", outlier.color = NA,
                    width = 0.6, lwd = 0.3, color = "black",
                    alpha = 0.8) +
  scale_fill_manual(values = c("#FFC107","#0051B9","black","#D81B1B"), name = "Phenotype") + 
  labs(x = "  ", y = parse(text = "F[ROH]")) + scale_x_discrete(labels = label_wrap(13))
pdf(file = "./ROH2.pdf")
froh_plot
dev.off()

### Figure 3 B and C
```{r}
library(ggplot2)

data <- data.frame(Category = rep(c("100-300", "300-500", ">500"), times = 5),
                   Value = c(813.5, 11.9, 1.9, 351.312, 15.625, 2.25, 134, 7.75, 0.75, 76.5385, 5.46154, 0.692, 73.875, 5.75, 0.875),
                   BarType = rep(c("Florida Keys", "Saint Pierre & Miquelon", "Anticosti", "North Mainland", "South Mainland"), each = 3))
data$Category <- factor(data$Category, levels = c("100-300", "300-500", ">500"))
print(data)

ggplot(data, aes(x = Category, y = Value, fill = BarType)) +
  geom_bar(stat = "identity", position = "dodge") +
  labs(x = "ROH Size Category (KB)",
       y = "Average Number of ROHs",
       fill = "Population") +
  scale_fill_manual(values = c("Florida Keys" = "#D81B1B", 
                                "Saint Pierre & Miquelon" = "#0051B9", 
                                "Anticosti" = "#DCCCA9",
                                "North Mainland" = "black",
                                "South Mainland" = "#717171"
                                )) +
  theme_bw()

data2 <- data.frame(Category = rep(c("100-300", "300-500", ">500"), times = 5),
                   Value = c(111317, 4474.29, 1424.68, 50672.7, 5958.61, 1507.15, 19025.6, 3073.36, 674.326, 10831.4, 2180.18, 572.953, 10722.3, 2230.18, 717.664),
                   BarType = rep(c("Florida Keys", "Saint Pierre & Miquelon", "Anticosti", "North Mainland", "South Mainland"), each = 3))
print(data2)
data2$Category <- factor(data$Category, levels = c("100-300", "300-500", ">500"))

ggplot(data2, aes(x = Category, y = Value, fill = BarType)) +
  geom_bar(stat = "identity", position = "dodge") +
  labs(x = "ROH Size Category (KB)",
       y = "Total ROH length (KB)",
       fill = "Population") +
  scale_fill_manual(values = c("Florida Keys" = "#D81B1B", 
                                "Saint Pierre & Miquelon" = "#0051B9", 
                                "Anticosti" = "#DCCCA9",
                                "North Mainland" = "black",
                                "South Mainland" = "#717171"
                                )) +
  theme_bw()
