### Figure 1 (R)
```{r}
library(ggplot2)
library(sf)
library(sp)
library(ggspatial)
library(rnaturalearth)
library(rnaturalearthdata)
library(rgeos)
library(readxl)
library(ggrepel)
library(tidyr)
library(dplyr)
library(viridis)


#### WTD ####
# Read & edit the data
WTD <- read_xlsx("./Deer_Samples_bcars.xlsx")

WTD$Lat <- as.numeric(WTD$Lat)
WTD$Long <- as.numeric(WTD$Long)
##### maps layers ##### 
# american statelines 
states <- ne_states(country = "united states of america")
# map layer of american statelines
states.layer <- geom_polygon(aes(x = long, y = lat, group = group),
                            data = states, fill = "white", 
                            color = "grey") 

# mexico
all_states <- map_data("world", region = "mexico") 
overlap_mexico <- geom_polygon(aes(x = long, y = lat, group = group),
                               data = all_states, fill = "white", 
                               color = "grey")

# Canada
provinces <- ne_states(country = "canada")
provinces.layer <- geom_polygon(aes(x = long, y = lat, group = group),
                                data = provinces, fill = NA, 
                                color = "grey")

# all other country borders
world <- ne_countries(scale = "medium", returnclass = "sf")

#Stagger overlapping points
jitter <- position_jitter(width = 0.5, height = 0.5)

##### Species map #####
pdf(file = "./TMAP5.pdf")
ggplot(world) + geom_sf(colour = "grey", fill = "white") + # colour = borders
  coord_sf(ylim = c(23.00, 60.00), xlim = c(-110.00, -50.00), expand = FALSE) + # zoom in
  states.layer + provinces.layer + # US & Canadian layers
  overlap_mexico + 
  geom_point(data = WTD, position = jitter, size = 2,
             aes(x = Long, y = Lat, color = Phenotypes)) + scale_colour_manual(values = c("#FFC107","#0051B9","#D81B1B","black"), breaks=c("Leucism", "Malocclusions", "Small Stature", "Healthy"), labels=c("Leucism", "Malocclusions", "Small Stature", "Normal")) +
 
  theme_minimal() +
  theme(axis.title.x = element_blank(),
        axis.text.x = element_blank(),
        axis.ticks.x = element_blank(),
        axis.title.y = element_blank(),
        axis.text.y = element_blank(),
        axis.ticks.y = element_blank(),
        legend.position = "bottom",
        legend.box="vertical",
        legend.title = element_blank()) +
  # Repel prevents labels from overlapping
  geom_text_repel(data = WTD, position = jitter, # Jitter staggers overlapping points for clarity
                  aes(x = Long, y = Lat,label = Genome_ID), 
                  color = "black", size = 1.5, max.overlaps = 100) 
dev.off()

#zoom in on SP
ggplot(world) + geom_sf(colour = "grey", fill = "white") + # colour = borders
  coord_sf(ylim = c(46.7, 47.2), xlim = c(-55.7, -56.8), expand = FALSE) + # zoom in
  states.layer + provinces.layer + # US & Canadian layers
  overlap_mexico + 
  # Actual data
  geom_point(data = WTD, size = 3,
             aes(x = Long, y = Lat, color = Phenotypes, shape = Location)) + scale_colour_manual(values = c("#FFC107","#0051B9","#D81B1B","black"), breaks=c("Leucism", "Malocclusions", "Small Stature", "Healthy"), labels=c("Leucism", "Malocclusions", "Small Stature", "Normal")) +
  scale_shape_manual(values=c(19, 19, 19, 19, 19), na.translate = TRUE, na.value = 2) +
  # geom_point(data = upcoming, position = jitter, size = 3, 
  #           aes(x = Long, y = Lat, color = "lightcoral")) + 

  theme_minimal() +
  theme(axis.title.x = element_blank(),
        axis.text.x = element_blank(),
        axis.ticks.x = element_blank(),
        axis.title.y = element_blank(),
        axis.text.y = element_blank(),
        axis.ticks.y = element_blank(),
        legend.position = "bottom",
        legend.box="vertical",
        legend.title = element_blank()) +
  # Repel prevents labels from overlapping
  geom_text_repel(data = WTD,
                  aes(x = Long, y = Lat,label = Genome_ID), 
                  color = "black", size = 2, max.overlaps = 100) 
#zoom in on keys
ggplot(world) + geom_sf(colour = "grey", fill = "white") + # colour = borders
  coord_sf(ylim = c(24.5, 24.9), xlim = c(-80.5, -82), expand = FALSE) + 
  states.layer + provinces.layer + # US & Canadian layers
  overlap_mexico + 
  # Actual data
  geom_point(data = WTD, size = 3,
             aes(x = Long, y = Lat, color = Phenotypes)) + scale_colour_manual(values = c("#FFC107","#0051B9","#D81B1B","black"), breaks=c("Leucism", "Malocclusions", "Small Stature", "Normal")) +

  theme_minimal() +
  theme(axis.title.x = element_blank(),
        axis.text.x = element_blank(),
        axis.ticks.x = element_blank(),
        axis.title.y = element_blank(),
        axis.text.y = element_blank(),
        axis.ticks.y = element_blank(),
        legend.position = "bottom",
        legend.box="vertical",
        legend.title = element_blank()) +
  # Repel prevents labels from overlapping
  geom_text_repel(data = WTD, 
                  aes(x = Long, y = Lat,label = Genome_ID), 
                  color = "black", size = 2, max.overlaps = 100) 
```

### Figures arranged with Inkscape`
