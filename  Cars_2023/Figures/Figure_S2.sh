### Figure S1 (R)
```{r}
library(tidyverse)
library(ggplot2)
pca <- read_table2("./DEER.eigenvec", col_names = FALSE)
eigenval <- scan("./DEER.eigenval")
pca <- pca[,-1]
# set names
names(pca)[1] <- "ind"
names(pca)[2:ncol(pca)] <- paste0("PC", 1:(ncol(pca)-1))

#add species
Phenotypes <- rep(NA, length(pca$ind))
Phenotypes[grep("AC2_S85_L004.unique_reads.bam", pca$ind)] <- "Healthy"
Phenotypes[grep("AC3_S86_L004.unique_reads.bam", pca$ind)] <- "Healthy"
Phenotypes[grep("AC4_S87_L004.unique_reads.bam", pca$ind)] <- "Healthy"
Phenotypes[grep("AC7_S88_L004.unique_reads.bam", pca$ind)] <- "Healthy"
Phenotypes[grep("Odo_AL3_S1_L001.unique_reads.bam", pca$ind)] <- "Healthy"
Phenotypes[grep("Odo_ME1_S11_L002.unique_reads.bam", pca$ind)] <- "Healthy"
Phenotypes[grep("Odo_NB1_S13_L008.unique_reads.bam", pca$ind)] <- "Healthy"
Phenotypes[grep("Odo_NC1_S5_L001.unique_reads.bam", pca$ind)] <- "Healthy"
Phenotypes[grep("Odo_NS1_S6_L001.unique_reads.bam", pca$ind)] <- "Healthy"
Phenotypes[grep("Odo_NY1_S3_L001.unique_reads.bam", pca$ind)] <- "Healthy"
Phenotypes[grep("Odo_NY2_S15_L002.unique_reads.bam", pca$ind)] <- "Healthy"
Phenotypes[grep("Odo_ON2R_S9_L008.unique_reads.bam", pca$ind)] <- "Healthy"
Phenotypes[grep("Odo_ON6_S12_L008.unique_reads.bam", pca$ind)] <- "Healthy"
Phenotypes[grep("Odo_ON_X1_S11_L004.unique_reads.bam", pca$ind)] <- "Healthy"
Phenotypes[grep("Odo_ON_X2_S12_L004.unique_reads.bam", pca$ind)] <- "Healthy"
Phenotypes[grep("Odo_ON_X3_S13_L004.unique_reads.bam", pca$ind)] <- "Healthy"
Phenotypes[grep("Odo_ON_X4_S14_L004.unique_reads.bam", pca$ind)] <- "Healthy"
Phenotypes[grep("Odo_QC1_S12_L008.unique_reads.bam", pca$ind)] <- "Healthy"
Phenotypes[grep("Odo_QC2_S_L001.merged.unique_reads.bam", pca$ind)] <- "Healthy"
Phenotypes[grep("Odo_SC1_S6_L001.unique_reads.bam", pca$ind)] <- "Healthy"
Phenotypes[grep("Odo_TX18_S9_L003.unique_reads.bam", pca$ind)] <- "Healthy"
Phenotypes[grep("Odo_TX20_S10_L003.unique_reads.bam", pca$ind)] <- "Healthy"
Phenotypes[grep("Odo_TX23_S17_L004.unique_reads.bam", pca$ind)] <- "Healthy"
Phenotypes[grep("Odo_TX3_S8_L003.unique_reads.bam", pca$ind)] <- "Healthy"
Phenotypes[grep("Odo_VA3_S14_L008.unique_reads.bam", pca$ind)] <- "Healthy"
Phenotypes[grep("KD15_S79_L004.unique_reads.bam", pca$ind)] <- "Small Stature"
Phenotypes[grep("KD22_S80_L004.unique_reads.bam", pca$ind)] <- "Small Stature"
Phenotypes[grep("KD29_S81_L004.unique_reads.bam", pca$ind)] <- "Small Stature"
Phenotypes[grep("KD33_S82_L004.unique_reads.bam", pca$ind)] <- "Small Stature"
Phenotypes[grep("KD46_S83_L004.unique_reads.bam", pca$ind)] <- "Small Stature"
Phenotypes[grep("KD49_S84_L004.unique_reads.bam", pca$ind)] <- "Small Stature"
Phenotypes[grep("Odo_Key1_S2_L001.unique_reads.bam", pca$ind)] <- "Small Stature"
Phenotypes[grep("Odo_Key3_S_L007.merged.unique_reads.bam", pca$ind)] <- "Small Stature"
Phenotypes[grep("Odo_Key4_S4_L001.unique_reads.bam", pca$ind)] <- "Small Stature"
Phenotypes[grep("Odo_Key5_S5_L001.unique_reads.bam", pca$ind)] <- "Small Stature"
Phenotypes[grep("SP016_S64_L004.unique_reads.bam", pca$ind)] <- "Leucism" 
Phenotypes[grep("SP030_S75_L004.unique_reads.bam", pca$ind)] <- "Healthy"
Phenotypes[grep("SP032_S68_L004.unique_reads.bam", pca$ind)] <- "Malocclusions"
Phenotypes[grep("SP035_S65_L004.unique_reads.bam", pca$ind)] <- "Leucism"
Phenotypes[grep("SP038_S69_L004.unique_reads.bam", pca$ind)] <- "Malocclusions"
Phenotypes[grep("SP042_S76_L004.unique_reads.bam", pca$ind)] <- "Healthy"
Phenotypes[grep("SP050_S77_L004.unique_reads.bam", pca$ind)] <- "Healthy"
Phenotypes[grep("SP051_S78_L004.unique_reads.bam", pca$ind)] <- "Healthy"
Phenotypes[grep("SP056_S70_L004.unique_reads.bam", pca$ind)] <- "Malocclusions"
Phenotypes[grep("SP069_S71_L004.unique_reads.bam", pca$ind)] <- "Malocclusions"
Phenotypes[grep("SP079_S66_L004.unique_reads.bam", pca$ind)] <- "Leucism"
Phenotypes[grep("SP084_S72_L004.unique_reads.bam", pca$ind)] <- "Malocclusions"
Phenotypes[grep("SP090_S73_L004.unique_reads.bam", pca$ind)] <- "Malocclusions"
Phenotypes[grep("SP093_S67_L004.unique_reads.bam", pca$ind)] <- "Leucism"
Phenotypes[grep("SP095_S74_L004.unique_reads.bam", pca$ind)] <- "Malocclusions"
Phenotypes[grep("Odo_SP008_S2_L001.unique_reads.bam", pca$ind)] <- "Healthy"

#add location
Location <- rep(NA, length(pca$ind))
Location[grep("AC", pca$ind)] <- "Anticosti"
Location[grep("Odo_ME1_S11_L002.unique_reads.bam", pca$ind)] <- "North Mainland"
Location[grep("Odo_NB1_S13_L008.unique_reads.bam", pca$ind)] <- "North Mainland"
Location[grep("Odo_NS1_S6_L001.unique_reads.bam", pca$ind)] <- "North Mainland"
Location[grep("Odo_NY1_S3_L001.unique_reads.bam", pca$ind)] <- "North Mainland"
Location[grep("Odo_NY2_S15_L002.unique_reads.bam", pca$ind)] <- "North Mainland"
Location[grep("Odo_ON2R_S9_L008.unique_reads.bam", pca$ind)] <- "North Mainland"
Location[grep("Odo_ON6_S12_L008.unique_reads.bam", pca$ind)] <- "North Mainland"
Location[grep("Odo_ON_X1_S11_L004.unique_reads.bam", pca$ind)] <- "North Mainland"
Location[grep("Odo_ON_X2_S12_L004.unique_reads.bam", pca$ind)] <- "North Mainland"
Location[grep("Odo_ON_X3_S13_L004.unique_reads.bam", pca$ind)] <- "North Mainland"
Location[grep("Odo_ON_X4_S14_L004.unique_reads.bam", pca$ind)] <- "North Mainland"
Location[grep("Odo_QC1_S12_L008.unique_reads.bam", pca$ind)] <- "North Mainland"
Location[grep("Odo_QC2_S_L001.merged.unique_reads.bam", pca$ind)] <- "North Mainland"
Location[grep("Odo_AL3_S1_L001.unique_reads.bam", pca$ind)] <- "South Mainland"
Location[grep("Odo_NC1_S5_L001.unique_reads.bam", pca$ind)] <- "South Mainland"
Location[grep("Odo_SC1_S6_L001.unique_reads.bam", pca$ind)] <- "South Mainland"
Location[grep("Odo_TX18_S9_L003.unique_reads.bam", pca$ind)] <- "South Mainland"
Location[grep("Odo_TX20_S10_L003.unique_reads.bam", pca$ind)] <- "South Mainland"
Location[grep("Odo_TX23_S17_L004.unique_reads.bam", pca$ind)] <- "South Mainland"
Location[grep("Odo_TX3_S8_L003.unique_reads.bam", pca$ind)] <- "South Mainland"
Location[grep("Odo_VA3_S14_L008.unique_reads.bam", pca$ind)] <- "South Mainland"
Location[grep("K", pca$ind)] <- "Florida Keys"
Location[grep("SP", pca$ind)] <- "Saint. Pierre"

# combine - if you want to plot each in different colours
Phenotypes_location <- paste0(Phenotypes, "_", Location)
pca <- as_tibble(data.frame(pca, Phenotypes, Location, Phenotypes_location)) 
#1:no. samples
pve <- data.frame(PC = 1:20, pve = eigenval/sum(eigenval)*100)

b <- ggplot(pca, aes(PC1, PC2, col = Phenotypes, shape = Location)) + geom_point(size = 3) + scale_shape_manual(values=c(8, 17, 19, 18, 15))
b <- b + scale_colour_manual(values = c("#FFC107","#0051B9","#D81B1B","black"), breaks=c("Leucism", "Malocclusions", "Small Stature", "Healthy"), labels=c("Leucism", "Malocclusions", "Small Stature", "Normal"))
pdf(file = "./PCA1.pdf")
b + xlab(paste0("PC1 (", signif(pve$pve[1], 3), "%)")) + ylab(paste0("PC2 (", signif(pve$pve[2], 3), "%)")) 
